import argparse
import logging
from pathlib import Path
from shutil import copyfile
from cachedb.ppcache import PPfilecache
from bblsuite.tools.shell import run_command


class Predictprotein:
    def __init__(self, args:list=[]):
        self.log = logging.getLogger(self.__class__.__name__)
        self.args = args
        # config
        self.REGISTRY = {
            'singularity': {
                'CONTAINER': '/projects/bromberg/singularity/predictprotein.sif',
                'CMD_RUN': 'run'
            },
            'docker': {
                'CONTAINER': 'bromberglab/predictprotein',
                'CMD_RUN': 'run'
            }
        }

    def run_container(self, job_uid, query_fasta:Path, targetlist, db_path:Path, in_folder:Path, out_folder:Path, runner) :
        in_folder.mkdir(parents=True, exist_ok=True)
        out_folder.mkdir(parents=True, exist_ok=True)
        if runner == 'docker':
            query_host = in_folder / query_fasta.name
            if query_fasta != query_host:
                copyfile(query_fasta, query_host)
            query_in = f'/mnt/local-storage/in/{query_fasta.name}'
            wd = f'/mnt/local-storage/out/{job_uid}_wd'
            out = f'/mnt/local-storage/out/{job_uid}_out'
        else:
            query_in = query_fasta
            wd = out_folder / f'{job_uid}_wd'
            out = out_folder / f'{job_uid}_out'

        cmd_base = [
            runner,
            self.REGISTRY[runner]['CMD_RUN']
        ]
        if runner == "docker":
            cmd_base += [
                '--rm',
                '-v', f'{in_folder}:/mnt/local-storage/in',
                '-v', f'{out_folder}:/mnt/local-storage/out',
                '-v', f'{db_path}:/usr/share/rostlab-data/data',
            ]
        cmd_base += [
            self.REGISTRY[runner]['CONTAINER'],
            'predictprotein',
            '--num-cpus=2',
            '--blast-processors=1',
            f'--seqfile="{query_in}"', # /in/{str(query_fasta)}
            f'--output-dir="{out}"', # /projects/bromberg/projects/snap3/out/{str(job_uid)}
            f'--work-dir="{wd}"' # /projects/bromberg/projects/snap3/wd/{str(job_uid)}
        ]
        cmd_base = cmd_base + self.args if self.args else cmd_base
        cmd = cmd_base + [f"--target={t}" for t in targetlist]
        self.log.debug(f'Executing: {cmd}')

        run_command(cmd)

    def run(self, job_uid, query_fasta:Path, targetlist, db_path:Path, ppcache_path:Path, in_folder:Path, out_folder:Path, runner, force):  
        # check for existing predictprotein results
        outputfolder_local = out_folder / f'{job_uid}_out'
        found_output = outputfolder_local.exists() and [str(p.parent) for p in outputfolder_local.rglob('*') if p.is_file() and p.name[0] != '.']
        use_cache = False
        if not force and found_output:
            self.log.info(f'Found existing PredictProtein results [{outputfolder_local}]')
            use_cache = True
        elif not force and ppcache_path:
            ppcache = PPfilecache(query_in=query_fasta, storage_path=ppcache_path)
            if ppcache.lookup():
                self.log.info(f'Retrieving existing PredictProtein cache...')
                ppcache.get(outputfolder_local, remove_existing=True)
                use_cache = True
        if not use_cache:
            self.log.info(f'Running PredictProtein...')
            self.run_container(job_uid, query_fasta, targetlist, db_path, in_folder, out_folder, runner)
            self.log.info(f'Done.')
        return outputfolder_local


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputfile', required=True, help="input file in fasta format")
    parser.add_argument('-j', '--jobid', required=True, help="unique jobid")
    parser.add_argument('-t', '--targets', default=['all'], nargs='+')
    parser.add_argument('-D', '--db_path', default=Path.cwd(), help="path to predictprotein database on host machine")
    parser.add_argument('-P', '--ppcache', default=Path.cwd(), help='path to predictprotein cache on host machine')
    parser.add_argument('-I', '--infolder', default=Path.cwd(), help="path to base mount for any input on host machine")
    parser.add_argument('-O', '--outfolder', default=Path.cwd(), help="path to base mount for any input on host machine")
    parser.add_argument('-R', '--runner', default="docker", help="container runner: docker or singularity")
    parser.add_argument('-f', '--force', default=False, help="force PredictProtein run")

    args, ppargs = parser.parse_known_args()
    query_in = Path(args.inputfile) if Path(args.inputfile).is_absolute() else Path(args.infolder) / args.inputfile

    pprotein = Predictprotein(ppargs)
    pprotein.run(
        job_uid=args.jobid,
        query_fasta=query_in,
        targetlist=args.targets,
        db_path=Path(args.db_path).absolute(),
        ppcache_path=Path(args.ppcache).absolute(),
        in_folder=Path(args.infolder).absolute(),
        out_folder=Path(args.outfolder).absolute(),
        runner=args.runner,
        force=args.force
    )
