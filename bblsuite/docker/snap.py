import argparse
import logging
import gzip
import re
from pathlib import Path
from shutil import copyfile
from cachedb.ppcache import PPfilecache
from bblsuite.tools.shell import run_command
from .predictprotein import Predictprotein
from Bio.SeqIO import read


logging.basicConfig(level=logging.DEBUG)


class Snap:
    def __init__(self, args:list=[]):
        self.log = logging.getLogger(self.__class__.__name__)
        self.args = args
        # (predictprotein_target, (predictprotein_cache_filename, coressponding_snap_intermediate_file_name))
        self.pprotein_targets = [
            ('blastPsiMat', {'.blastPsiMat': '.asci', '.blastPsiRdb': '.blastPsiRdb'}),     # asci
            ('blastPsiOutTmp', {'.blastPsiOutTmp': '.blast'}),                              # -> snap filter/extract
            ('clustalngz', {'.clustalngz': '.clustal.gz'}),                                 # -> snap filter/extract
            ('hmm2pfam', {'.hmm2pfam': '.pfam'}),                                           # pfam
            ('hsspPsiFil', {'.hsspPsiFil.gz': '-fil.hssp'}),                                # -> snap filter/extract
            ('prof1Rdb', {'.prof1Rdb': '.rdbProf'}),                                        # prof_n
            ('profb4snap', {'.profb4snap': '.profbval'}),                                   # bval
            ('profRdb', {'.profRdb': '-fil.rdbProf'}),                                      # prof_a
            ('psic', {'.psic': '.out'})                                                     # psic
                                                                                            # sift:  -> snap filter/extract
                                                                                            # swiss: -> snap filter/extract
        ]

        # if ($checkpsic eq "blastpgp: No hits found") {
		# 			if (!($__tolerate_psic_failure)){
		# 				confess( "'Error - $checkpsic for PSIC'" )
		# 			}
		# 			else {
		# 				my @cmd = ("touch", "$__workdir/$__jobname/$__jobname.out" );
		# 				cluck("@cmd") if ($debug); 
		# 				system(@cmd) && confess("failed to '@cmd' ".( $?>>8 ))
		# 			}
		# 		}

        # config
        self.REGISTRY = {
            'singularity': {
                'CONTAINER': '/projects/bromberg/singularity/snap.sif',
                'CMD_RUN': 'run'
            },
            'docker': {
                'CONTAINER': 'bromberglab/snap',
                'CMD_RUN': 'run'
            }
        }

    def run_container(self, job_uid, query_fasta:Path, mutations_path:Path, db_path:Path, in_folder:Path, out_folder:Path, runner) :
        in_folder.mkdir(parents=True, exist_ok=True)
        Path(out_folder / job_uid).mkdir(parents=True, exist_ok=True)
        if runner == 'docker':
            query_host = out_folder / job_uid / f'{job_uid}.fasta'
            copyfile(query_fasta, query_host)
            query_in = Path('/output/') / job_uid / f'{job_uid}.fasta'
            mutations_host = in_folder / mutations_path.name
            if mutations_path != mutations_host:
                mutations_moced = out_folder / mutations_path.name
                if mutations_path != mutations_moced:
                    copyfile(mutations_path, mutations_host)
                mutations_in = Path('/output/') / mutations_path.name
            else:
                mutations_in = Path('/input/') / mutations_path.name
            wd = Path('/output/')
            out = Path('/output/') / f'{job_uid}.out'
        else:
            query_uid = out_folder / job_uid / f'{job_uid}.fasta'
            copyfile(query_fasta, query_uid)
            query_in = query_uid
            mutations_in = mutations_path
            wd = out_folder
            out = out_folder / f'{job_uid}.out'

        cmd_base = [
            runner,
            self.REGISTRY[runner]['CMD_RUN']
        ]
        if runner == "docker":
            cmd_base += [
                '--rm',
                '-v', f'{in_folder}:/input',
                '-v', f'{out_folder}:/output',
                '-v', f'{db_path}:/usr/share/rostlab-data/data',
            ]
        cmd_base += [
            self.REGISTRY[runner]['CONTAINER'],
            '-i', str(query_in),
            '-m', str(mutations_in),
            '-o', str(out),
            f'--workdir={wd}'
        ]
        cmd = cmd_base + self.args if self.args else cmd_base
        self.log.debug(f'Executing: {cmd}')
        run_command(cmd, logger=self.log)

    def check_targets(self, ppcache, out_folder:Path, prefix="query"):
        if not ppcache.lookup():
            return [target for target, ppcache_files in self.pprotein_targets]
        out_folder.mkdir(parents=True, exist_ok=True)
        missing_targets = []
        for target, ppcache_files in self.pprotein_targets + [('in', {'.in': '_query.fasta'})]:
            target_incomplete = False
            for pprotein_fname, snap_fname in ppcache_files.items():
                snap_fname = snap_fname if snap_fname else pprotein_fname
                pprotein_src = ppcache.seguid_path / f'{ppcache.prefix}{pprotein_fname}'
                snap_target = out_folder / f'{prefix}{snap_fname}'
                if pprotein_src.exists():
                    self.log.debug(f'[{target}] copying: {pprotein_src}')
                    if snap_target.suffix == '.gz':
                        snap_target_compressed = out_folder / f'{prefix}{snap_fname}'
                        snap_target = out_folder / f'{snap_target.stem}'
                        copyfile(pprotein_src, snap_target_compressed)
                        self.log.debug(f'[{target}] uncompressing: {snap_target_compressed.name}')
                        with gzip.open(snap_target_compressed, 'rb') as fin, snap_target.open('wb') as fout:
                            fout.write(fin.read())
                    elif pprotein_src.suffix == '.gz':
                        snap_target_compressed = out_folder / f'{ppcache.prefix}{pprotein_fname}'
                        copyfile(pprotein_src, snap_target_compressed)
                        self.log.debug(f'[{target}] uncompressing: {snap_target_compressed.name}')
                        with gzip.open(snap_target_compressed, 'rb') as fin, snap_target.open('wb') as fout:
                            fout.write(fin.read())
                    else:
                        copyfile(pprotein_src, snap_target)
                else:
                    self.log.debug(f'[{target}] missing result: {pprotein_fname} -> {snap_fname}')
                    if not target_incomplete:
                        missing_targets += [target]
                        target_incomplete = True
            if target_incomplete:
                self.log.debug(f'Adding target {target} to PredictProtein run')
        self.log.info(f'Retrieved targets from cache: {len(self.pprotein_targets)-len(missing_targets)}/{len(self.pprotein_targets)}')
        return missing_targets

    def check_mutations(self, job_uid, query_fasta:Path, mutations_path:Path, out_folder:Path):
        query_seq = read(query_fasta, 'fasta').seq
        pattern_mut = re.compile(r'^\s*([A-Z])([0-9]+)([A-Z])\s*$')
        mutations_checked = []
        mutations_altered = False
        with mutations_path.open() as fin:
            for line in fin.readlines():
                for match in re.finditer(pattern_mut, line):
                    res, pos, mut = match.groups()
                    pos_int = int(pos)
                    if 0 < pos_int <= len(query_seq):
                            query_seq_res = query_seq[pos_int-1]
                            if query_seq_res == res:
                                mutations_checked.append((res, pos, mut))
                            else:
                                mutations_checked.append((query_seq_res, pos, mut))
                                mutations_altered = True
                                self.log.warn(f'Corrected invalid mutation [{res}{pos}{mut}] to [{query_seq_res}{pos}{mut}] based on residue at query position: {pos}')
                    else:
                        mutations_altered = True
                        self.log.warn(f'Skipped invalid mutation [{res}{pos}{mut}] due to invalid query position: {pos}')
        if mutations_altered:
            mutations_path_updated = out_folder / f'{job_uid}.mut'
            with mutations_path_updated.open('w') as fout:
                fout.writelines([ f'{"".join(mut)}\n' for mut in mutations_checked ])
            return mutations_path_updated
        return mutations_path

    def run(self, job_uid, query_fasta:Path, mutations_path:Path, db_path:Path, ppcache_path:Path, in_folder:Path, out_folder:Path, runner, force):  
        outputfolder_local = out_folder / f'{job_uid}'
        if not force and ppcache_path:
            ppcache = PPfilecache(query_in=query_fasta, storage_path=ppcache_path)
            missing_targets = self.check_targets(ppcache, outputfolder_local, prefix=job_uid)
            if missing_targets:
                pprotein_out = self.run_pprotein(job_uid, query_fasta, db_path=db_path, ppcache_path=None ,in_folder=in_folder, out_folder=outputfolder_local, runner=runner, force=force)
                ppcache.store(pprotein_out, action='merge')
                missing_targets = self.check_targets(ppcache, outputfolder_local, prefix=job_uid)
        mutations_path = self.check_mutations(job_uid, query_fasta, mutations_path, out_folder)
        self.log.info(f'Running SNAP...')
        self.run_container(job_uid, query_fasta, mutations_path, db_path, in_folder, out_folder, runner)
        return (outputfolder_local, out_folder / f'{job_uid}.out')

    def run_pprotein(self, job_uid, query_fasta:Path, db_path:Path, ppcache_path:Path, in_folder:Path, out_folder:Path, runner, force):
        pprotein = Predictprotein()
        query_targets = [f'query.{t}' for t, f in self.pprotein_targets]
        pprotein_out = pprotein.run(job_uid, query_fasta, query_targets, db_path, ppcache_path, in_folder, out_folder, runner, force)
        return pprotein_out


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputfile', required=True, help="input file in fasta format")
    parser.add_argument('-j', '--jobid', required=True, help="unique jobid")
    parser.add_argument('-m', '--mutations', required=True, help='path to file with mutations of interest')
    parser.add_argument('-D', '--db_path', default=Path.cwd(), help="path to predictprotein database on host machine")
    parser.add_argument('-P', '--ppcache', default=Path.cwd(), help='path to predictprotein cache on host machine')
    parser.add_argument('-I', '--infolder', default=Path.cwd(), help="path to base mount for any input on host machine")
    parser.add_argument('-O', '--outfolder', default=Path.cwd(), help="path to base mount for any input on host machine")
    parser.add_argument('-R', '--runner', default="docker", help="container runner: docker or singularity")
    parser.add_argument('-f', '--force', default=False, help="force SNAP run")

    args, snapargs = parser.parse_known_args()
    query_in = Path(args.inputfile) if Path(args.inputfile).is_absolute() else Path(args.infolder) / args.inputfile
    mutations = Path(args.mutations) if Path(args.mutations).is_absolute() else Path(args.infolder) / args.mutations

    snap = Snap(snapargs)
    results_folder, predictions = snap.run(
        job_uid=args.jobid,
        query_fasta=query_in,
        mutations_path=mutations,
        db_path=Path(args.db_path).absolute(),
        ppcache_path=Path(args.ppcache).absolute(),
        in_folder=Path(args.infolder).absolute(),
        out_folder=Path(args.outfolder).absolute(),
        runner=args.runner,
        force=args.force
    )
    return (results_folder, predictions)


if __name__ == "__main__":
    main()
