import argparse
import datetime
import time
import threading
from os import environ, getenv
from .. import globals, logger, config
from .cluster import Cluster, Project, Job
from .states import DaemonState
from .submitter import Submitter, JobState


log = logger.Logger.get_logger()
configuration = config.Config()


def db_reset_locked_jobs(db, status, revert):
    """Get locked jobs from database"""

    resultset = db.query('SELECT id FROM jobs WHERE daemon=%i' % status)
    db.update(
        'UPDATE jobs SET daemon=%i WHERE daemon=%i' % (revert, status))
    return resultset


def db_get_active_projects(db):
    """Get active project jobs from db"""

    resultset = db.query(
        f'SELECT DISTINCT(project), name, valid_clusters, transfer_dir, script, dbstore FROM projects, '
        f'project_jobs WHERE projects.id = project_jobs.project AND status < {DaemonState.TRANSFERRED.value:d}; '
    )
    available_clusters = Cluster.get_enabled_clusters_from_config()
    projects = []
    for res in resultset:
        if res[2] is None or '':
            valid_clusters = []
        elif res[2] == ".all":
            valid_clusters = available_clusters
        else:
            valid_clusters = []
            for cluster_name in res[2].split(','):
                valid_clusters.append(Cluster(cluster_name))
        registered_project = Project.create_registered(int(res[0]), res[1], valid_clusters,
                                                       res[3], res[4], bool(int(res[5])))
        projects += [registered_project]
    return projects


def auto_load_balance(jobs=None, db=None, projects=None, clusters=None, _timer=None, _arrayjob_size=None):
    """Auto balance jobs between available clusters"""

    active_project_submitters = {}
    active_clusters = {}

    if jobs is None and db is None:
        log.info('No jobs to submit!')
        return

    # --- reset locked jobs ---
    locked_fetched = len(db_reset_locked_jobs(db, DaemonState.FETCHED.value, DaemonState.REGISTERED.value))
    log.info("Number of locked fetched jobs reverted to inital state: %i" % locked_fetched)
    locked_transferring = len(db_reset_locked_jobs(db, DaemonState.TRANSFERRING.value, DaemonState.VALIDATED.value))
    log.info("Number of locked transferring jobs reverted to inital state: %i" % locked_transferring)

    while True:
        # read current config, update settings
        configuration.update()
        if _timer:
            timer = _timer
        else:
            timer = int(configuration.get_property('clubber', 'autobal.timer.sec'))
        if _arrayjob_size:
            arrayjob_size = _arrayjob_size
        else:
            arrayjob_size = int(configuration.get_property('clubber', 'arrayjob.size'))

        # Check for control signal
        ALB_SIGNAL = configuration.get_property('clubber', 'control') if configuration.has_property('clubber', 'control') else None
        
        if ALB_SIGNAL and str(ALB_SIGNAL) == "HALT":
            log.info(f'Got signal [{str(ALB_SIGNAL)}], shutting down...')
            break
        elif "CLUBBER_SHUTDOWN" in environ and getenv("CLUBBER_SHUTDOWN"):
            log.info(f'Got shutdown signal via ENV: {str(getenv("CLUBBER_SHUTDOWN"))}')
            break

        log.debug('Start auto load balancing run')
        start = time.time()

        active_clusters_updated = {}
        enabled_clusters_names = []
        if not clusters:
            enabled_clusters = Cluster.get_enabled_clusters_from_config()
        else:
            enabled_clusters = clusters
        for cluster in enabled_clusters:
            enabled_clusters_names.append(cluster.name)
            if cluster.name not in active_clusters.keys():
                active_clusters_updated[cluster.name] = cluster
                cluster.connect()
        for cluster in active_clusters.values():
            if cluster.name in enabled_clusters_names:
                active_clusters_updated[cluster.name] = cluster
                cluster.connect()
            else:
                cluster.disconnect()
        active_clusters = active_clusters_updated

        active_projects_submitters_updated = {}
        enabled_project_names = []
        active_projects_list = []
        if not projects:
            enabled_projects = db_get_active_projects(db)
        else:
            enabled_projects = projects
        for project in enabled_projects:
            enabled_project_names.append(project.name)
            active_projects_list.append(project)
        for project_name, project_submitter in active_project_submitters.items():
            if project_name in enabled_project_names:
                active_projects_submitters_updated[project_name] = project_submitter
        active_project_submitters = active_projects_submitters_updated

        log.info(f'AutoLoadBalancer: [ {", ".join(enabled_project_names)} ]')
        for project in active_projects_list:
            log.debug(f' * {project.name:10} > starting load balacing run...')
            enabled_project_clusters = []
            for project_cluster in project.clusters:
                for active_cluster in active_clusters.values():
                    if project_cluster.name.lower() == active_cluster.name.lower():
                        enabled_project_clusters.append(active_cluster)

            if project.name in active_project_submitters.keys():
                project_submitter = active_project_submitters.get(project.name)
                project_submitter.project = project
                project_submitter.cluster_list = enabled_project_clusters
            else:
                project_submitter = Submitter(project=project, cluster_list=enabled_project_clusters, db=db)
                project_submitter.add_threadlock(threading.Lock())
                active_project_submitters[project.name] = project_submitter

            # TODO project specific array job sizes
            globals.RUN_VARS['CLUBBER_ARRAY_SIZE'] = arrayjob_size
            limit = globals.RUN_VARS['CLUBBER_ARRAY_SIZE'] * len(enabled_project_clusters) * 2

            # update project jobs status
            log.debug(f' * {project.name:10} > updating project jobs status...')
            project_jobs_details = project_submitter.db_update_project_job_status()

            # cleanup
            log.debug(f' * {project.name:10} > starting cleanup...')
            for project_job_id, details in sorted(project_jobs_details.items()):
                status = details[0]
                progress = details[1]
                if status == JobState.FINISHED.value and progress == 100:
                    # TODO handle jobs with state JobState.FAILED.value
                    for cluster in enabled_project_clusters:
                        if project_submitter.cleanup_pool._work_queue.qsize() < \
                                project_submitter.cleanup_pool._max_workers:
                            project_submitter.cleanup_pool.submit(
                                project_submitter.duplicate().cleanup,
                                cluster.name,
                                project_job_id
                            )

            # post processing
            log.debug(f' * {project.name:10} > starting post processing...')
            for project_job_id, details in sorted(project_jobs_details.items()):
                status = details[0]
                progress = details[1]
                if status == JobState.FINISHED.value and progress == 100:
                    # TODO handle jobs with state JobState.FAILED.value
                    log.info(f" * {project.name:10} > {project_job_id:7d} > finished")
                    # TODO implement: start postprocessing

            log.debug(f' * {project.name:10} > getting active jobs...')
            active_unfinished_jobs_count = project_submitter.db_get_active_jobs_count()

            if active_unfinished_jobs_count:
                # input transfer and validation
                if enabled_project_clusters:
                    log.debug(f' * {project.name:10} > processing {active_unfinished_jobs_count:d} unfinished jobs...')
                for cluster in enabled_project_clusters:
                    validation_pool_current = project_submitter.validation_pool._work_queue.qsize()
                    validation_pool_max = project_submitter.validation_pool._max_workers
                    log.debug(
                        f' + {cluster.name:10} > {project.name:15} > '
                        f'{validation_pool_current:d}/{validation_pool_max} slots used in validation pool queue'
                    )
                    if validation_pool_current < validation_pool_max:
                        log.debug(f' + {cluster.name:10} > {project.name:15} > submitting to validation pool...')
                        project_submitter.validation_pool.submit(
                            project_submitter.duplicate().validate_and_transfer,
                            cluster.name
                        )
            else:
                log.debug(f' * {project.name:10} > no active jobs, skipping validation/transfer')

            if project_submitter.db is not None:
                log.debug(f' * {project.name:10} > fetching jobs from database [limit: {limit:d}]')
                jobs = project_submitter.db_get_registered_jobs(limit)

            if len(jobs) == 0:
                log.debug(f' * {project.name:10} > no new jobs in database')
            else:
                if project_submitter.submission_pool._work_queue.qsize() < \
                        project_submitter.submission_pool._max_workers:
                    if enabled_project_clusters:
                        log.info(f' * {project.name:10} > processing {len(jobs):d} jobs')
                    for cluster in enabled_project_clusters:
                        if len(jobs) == 0:
                            log.debug(
                                f' + {cluster.name:10} > {project.name:15} > '
                                f'skipping submission - no more jobs to submit'
                            )
                        else:
                            # get current jobs
                            job_list = project_submitter.get_cluster_job_states(cluster)
                            total = job_list['total']
                            pending = job_list['pending']

                            # decide if new jobs are submitted
                            array_size = globals.RUN_VARS['CLUBBER_ARRAY_SIZE']
                            percentage_threshold = float(
                                configuration.get_property('clubber', 'queue.submit.threshold')
                            )
                            submission_threshold = array_size * percentage_threshold

                            all_submitted = False
                            cluster_limit_reached = False
                            while pending <= submission_threshold:
                                if cluster.queuelimit >= total + globals.RUN_VARS['CLUBBER_ARRAY_SIZE']:
                                    jobs_to_submit = jobs[:globals.RUN_VARS['CLUBBER_ARRAY_SIZE']]
                                    new_jobs_count = len(jobs_to_submit)
                                    if new_jobs_count == 0:
                                        all_submitted = True
                                        log.info(f' + {cluster.name:10} > {project.name:15} > no more jobs to submit')
                                        break
                                    else:
                                        pending += new_jobs_count
                                        total += new_jobs_count

                                    # split jobs into different projects
                                    jobs_per_project_id = {}
                                    for job in jobs_to_submit:
                                        if not job.project:
                                            if 'None' not in jobs_per_project_id.keys():
                                                jobs_per_project_id['None'] = {0: [job]}
                                            else:
                                                jobs_per_project_id['None'][0].append(job)
                                        elif not job.project.subid:
                                            if job.project.name not in jobs_per_project_id.keys():
                                                jobs_per_project_id[job.project.name][0] = [job]
                                            else:
                                                jobs_per_project_id[job.project.name][0].append(job)
                                        else:
                                            if job.project.name not in jobs_per_project_id.keys():
                                                jobs_per_project_id[job.project.name] = {job.project.subid: [job]}
                                            else:
                                                if job.project.subid not in \
                                                        jobs_per_project_id[job.project.name].keys():
                                                    jobs_per_project_id[job.project.name][job.project.subid] = [job]
                                                else:
                                                    jobs_per_project_id[job.project.name][job.project.subid].append(job)

                                    for project_jobs_sublist in jobs_per_project_id.values():
                                        # TODO Lock jobs earlier and to prevent double fetching
                                        job_sublist = []
                                        for project_job_id, project_job_jobs in sorted(project_jobs_sublist.items()):
                                            job_sublist.extend(project_job_jobs)
                                        project_submitter.db_update_array_job_deamon_status(
                                            job_sublist, DaemonState.FETCHED.value
                                        )
                                        project_submitter.submission_pool.submit(
                                            project_submitter.duplicate().register_jobs,
                                            cluster.name,
                                            job_sublist
                                        )

                                    jobs = jobs[globals.RUN_VARS['CLUBBER_ARRAY_SIZE']:]
                                else:
                                    cluster_limit_reached = True
                                    log.info(f' + {cluster.name:10} > {project.name:15} > {total} jobs queued, '
                                             f'no submission due to cluster queue limit')
                                    break

                            if not all_submitted and not cluster_limit_reached:
                                log.info(
                                    f' + {cluster.name:10} > '
                                    f'{pending} jobs pending, no submission due to pending threshold'
                                )
                else:
                    log.debug(f' * {project.name:10} > submission queue full')

        end = time.time()
        duration = datetime.datetime.fromtimestamp(end - start).strftime('%M:%S')
        log.debug('Processed auto load balancing run in %s' % duration)

        if timer > 0:
            time.sleep(timer)

    log.info('Exiting auto load balancing - all jobs submitted')


def get_argument_parser():
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='Submit jobs to clubber')
    parser.add_argument('-p', '--project', metavar='project_name', const=None,
                        help='defines default working directory and job names (may be overwritten by job commands)')
    parser.add_argument('-j', '--job', metavar='"...job_1 details..." [-j "...job_2 details..."]', action='append',
                        help='jobs to submit')
    parser.add_argument('-c', '--cluster', metavar='cluster1 [-c cluster2]', action='append',
                        help='Clusters to use submitting the given jobs')
    parser.add_argument('-b', '--balance', metavar='load_balance_limit', const=None,
                        help='auto load balance limit (max number of queued jobs) [default: no auto balancing]')
    # parser.add_argument('-c', '--cluster', metavar='cluster1 [-c cluster2]', action='append',
    #                     help='enabled clusters')

    # parser.add_argument('-d', '--database', metavar='database', type=str, required=False, default=None,
    #                     help='job database')

    # parser.add_argument('-a', '--arrayjobsize', metavar='array_jobsize', type=int, default=100,
    #                         help='size of arrayjobs to submit per load balancing round, default: 100')

    # parser.add_argument('-t', '--timer', metavar='timer_seconds', type=int, default=60,
    #                     help='timer in seconds for auto balancing runs, default: 60')

    # parser.add_argument('-p', '--project', metavar='project_name', default=None,
    #                     help='defines default working directory and job names (may be overwritten by job commands)')

    # parser.add_argument('-i', '--inputdir', metavar='input_directory', required=False, default=None,
    #                     help='location of input files relative to --workingdir')

    # parser.add_argument('-w', '--workingdir', metavar='workingdir_directory', required=False, default=None,
    #                     help='working directory for job precessing, outputs, etc.')

    # parser.add_argument('-l', '--loggingdir', metavar='logging_directory', required=False, default='/tmp',
    #                     help='logging directory for daemon, default: /tmp')

    # parser.add_argument('-r', '--transferdir', metavar='transfer_directory', required=False, default=None,
    #                    help='limits transferred files to those included in the given directory'
    #                         '(relative to output directory, NOT MORE THAN 2 LEVELS DOWN !!!)')

    # parser.add_argument('-f', '--filetransfer', action='store_true',
    #                     help='if set resultfiles are automatically retrieved and stored in <workingdir>')
    return parser


def main(arguments):
    project_name = arguments.project
    jobs_args = arguments.job
    clusters_args = arguments.cluster
    balance_args = int(arguments.balance)

    project = None
    if arguments.project is not None:
        project = Project(project_name)

    jobs = []
    for job_cmd in jobs_args:
        job = Job(project=project, cmd=job_cmd)
        jobs += [job]

    clusters = []
    for cluster_name in clusters_args:
        clusters += [Cluster(cluster_name)]

    s = Submitter(project=project, cluster_list=clusters)

    if balance_args is None:
        s.submit_jobs(jobs)
    else:
        auto_load_balance(_arrayjob_size=balance_args)

    job_ids = list()
    for job in jobs:
        job_ids += [job.job_id]

    return job_ids
