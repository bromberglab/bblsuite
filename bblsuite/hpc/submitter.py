import time
import sys
import os
import shutil
import subprocess
from concurrent.futures import ThreadPoolExecutor
from .. import config, logger
from ..network import mysql, ssh
from ..io import fsutils
from ..tools import stringutils
from .cluster import Cluster, Project, Project_Job, Job
from .validation import ProjectValidator
from .states import DaemonState, JobState


__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class Submitter:
    """Submit jobs to the hpc submission system and retrieve details about running jobs"""

    def __init__(self, project=None, cluster_list=None, db=None, timezone=None, laravel=False, remote=False,
                 project_name=None, force_no_handler=False):
        self.log = logger.Logger.get_logger(force_no_handler=force_no_handler)
        self.config = config.Config()
        if project:
            self.project = project
        elif project_name:
            self.project = None
            if not cluster_list:
                cluster_list = Cluster.get_enabled_clusters_from_config()
        else:
            self.project = Project(name=self.config.get_property('clubber', 'default.project'),
                                   projectid=int(self.config.get_property('clubber', 'default.project.id')),
                                   valid_clusters=Cluster.get_enabled_clusters_from_config()
                                   )
        self.priority = 0
        self.cluster_list = []
        if cluster_list is None:
            self.cluster_list = Cluster.get_enabled_clusters_from_config()
        else:
            for cluster in cluster_list:
                if isinstance(cluster, str):
                    self.add_cluster(Cluster(cluster))
                else:
                    self.add_cluster(cluster)
        if isinstance(db, str):
            self.db = mysql.MySQLHandler(db)
        else:
            self.db = db
        if timezone:
            self.timezone = timezone
        else:
            self.timezone = self.config.get_property('clubber', 'timezone')
        self.submission_pool = ThreadPoolExecutor(int(self.config.get_property('clubber', 'threads.submission')))
        self.validation_pool = ThreadPoolExecutor(int(self.config.get_property('clubber', 'threads.validation')))
        self.transfer_pool = ThreadPoolExecutor(int(self.config.get_property('clubber', 'threads.transfer')))
        self.cleanup_pool = ThreadPoolExecutor(int(self.config.get_property('clubber', 'threads.cleanup')))

        if laravel:
            self.laravel_base = self.config.get_property('General', 'laravel.base')
        else:
            self.laravel_base = None

        self.remote = remote
        self.project_name = project_name
        self.threadlock = None

    def init(self, files):
        if not self.project:
            self.project = Project(self.project_name, projectid=self.db_get_project_id(self.project_name))
        if not self.remote:
            pass
        else:
            if files:
                clubber_wd = self.config.get_property('clubber', 'local.working.dir')
                clubber_in_dir = self.config.get_property('clubber', 'local.input.dir')
                clubber_out_dir = self.config.get_property('clubber', 'local.output.dir')
                self.project.subid = self.db_create_project_job()

                clubber_job_base = os.path.join(
                    clubber_wd, os.path.join(
                        self.project_name, str(self.project.subid)
                    )
                )
                clubber_job_in = os.path.join(clubber_job_base, clubber_in_dir)
                clubber_job_out = os.path.join(clubber_job_base, clubber_out_dir)
                clubber_job_tmp = os.path.join(clubber_job_base, 'tmp')

                clubber = ssh.SSH('clubber')
                clubber.connect(open_channel=True)
                clubber.send_shell(f'mkdir -m 775 {clubber_job_base}')
                clubber.send_shell(f'mkdir -m 775 {clubber_job_in}')
                clubber.send_shell(f'mkdir -m 775 {clubber_job_out}')
                clubber.send_shell(f'mkdir -m 775 {clubber_job_tmp}')
                clubber.sftp_open()
                for file in files:
                    file_name = os.path.basename(file)
                    dest = os.path.join(clubber_job_in, file_name)
                    clubber.sftp_put(file, dest)
                clubber.sftp_close()
                clubber.disconnect()

    def destroy(self):
        self.db.disconnect()

    def duplicate(self):
        cluster_list = []
        for c in self.cluster_list:
            cluster_list.append(Cluster(c.name))
        project = self.project.duplicate()
        if self.db:
            db = self.db.duplicate()
        else:
            db = None
        s = Submitter(project, cluster_list, db, self.timezone, self.laravel_base, self.remote, self.project_name)
        s.add_threadlock(self.threadlock)
        s.transfer_pool = self.transfer_pool
        return s

    def update_config(self):
        self.config.update()

    def add_threadlock(self, threadlock):
        self.threadlock = threadlock

    def set_priority(self, priority):
        self.priority = priority

    def create_project(self, script_path=None, source_archive_path=None, project_id=None):
        """Register project in db and copy to clusters"""
        # TODO copy to new clusters when new clusters are added any time after project was created

        if project_id:
            self.project = self.db_get_project(project_id)
            script_path = self.laravel_base + 'project_script_files/' + str(self.project.id) + '/' + self.project.script
            if self.project.sources is not None:
                source_archive_path = self.laravel_base + 'project_source_files/'\
                                      + str(self.project.id) + '/' + self.project.sources
        else:
            self.project.id = self.db_create_project()

        submitwrapper_path = os.path.join(os.path.dirname(__file__), 'interfaces')
        project_cluster_params_path = self.laravel_base + 'project_cluster_parameters/' + str(self.project.id) + '/'
        # clubbersh_path = self.laravel_base + 'project_script_files/' + str(
        #     self.project.id) + '/' + self.project.submit_script

        local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
        if not os.path.exists(local_project_basedir):
            fsutils.makedir(local_project_basedir, parents=True, mode=0o775, exist_ok=True)
        local_projectsh_path = os.path.join(local_project_basedir, self.project.submit_script)

        # if self.project.transfer:
        #     local_input_dir = os.path.join(local_project_basedir, self.project.local_input_dir)
        #     if not os.path.exists(local_input_dir):
        #         os.mkdir(local_input_dir)
        #     local_output_dir = os.path.join(local_project_basedir, self.project.local_output_dir)
        #     if not os.path.exists(local_output_dir):
        #         os.mkdir(local_output_dir)

        for cluster in self.project.clusters:

            remote_project_basedir = os.path.join(cluster.basedir, self.project.name)
            remote_input_dir = os.path.join(remote_project_basedir, self.project.remote_input_dir)
            remote_output_dir = os.path.join(remote_project_basedir, self.project.remote_output_dir)
            remote_clubbersh_path = os.path.join(remote_project_basedir, self.project.submit_script)
            remote_script_path = os.path.join(remote_project_basedir, self.project.script)
            if self.project.sources:
                remote_sources_path = os.path.join(remote_project_basedir, self.project.sources)
            else:
                remote_sources_path = remote_project_basedir

            submitwrapper = os.path.join(submitwrapper_path, "%s.sh" % cluster.load_manager_type.name)
            cluster_params = os.path.join(project_cluster_params_path, "%s.args" % cluster.name)
            params = ''
            with open(local_projectsh_path, 'w') as out:
                if os.path.exists(cluster_params):
                    with open(cluster_params) as fp:
                        for line in fp:
                            params += line.rstrip() + "\n"
                else:
                    params = '#no parameters'
                with open(submitwrapper) as fp:
                    for line in fp:
                        edited_line = line.rstrip()
                        if line.startswith('#PARAMS'):
                            edited_line = params
                        elif line.startswith('wd='):
                            edited_line = 'wd=%s' % remote_output_dir
                        elif line.startswith('inputdir='):
                            edited_line = 'inputdir=%s' % remote_input_dir
                        elif line.startswith('script='):
                            edited_line = 'script=%s' % remote_script_path
                        elif line.startswith('sources='):
                            edited_line = 'sources=%s' % remote_sources_path
                        out.write(edited_line + "\n")

            cluster.connect()

            cluster.ssh.send_shell('rm -rf %s' % remote_project_basedir)
            cluster.ssh.send_shell('mkdir %s' % remote_project_basedir)
            cluster.ssh.send_shell('mkdir %s' % remote_input_dir)
            cluster.ssh.send_shell('mkdir %s' % remote_output_dir)

            cluster.ssh.sftp_open()
            cluster.ssh.sftp_put(local_projectsh_path, remote_clubbersh_path)
            cluster.ssh.send_shell('chmod 755 %s' % remote_clubbersh_path)
            cluster.ssh.sftp_put(script_path, remote_script_path)
            cluster.ssh.send_shell('chmod 755 %s' % remote_script_path)
            if source_archive_path:
                cluster.ssh.sftp_put(source_archive_path, '%s/sources.zip' % remote_project_basedir)
                cluster.ssh.send_shell('cd %s; unzip sources.zip' % remote_project_basedir)

            cluster.disconnect()

    def submit(self, files=None, jobfile_path=None, arrayargsfile_path=None, arguments=None, array_args=None,
               array_files=None, appending=False):
        if self.db.connect():
            self.init(files)
            self.create_project_job(project_id=self.project.id, project_job_id=self.project.subid,
                                    jobfile_path=jobfile_path, arrayargsfile_path=arrayargsfile_path,
                                    arguments=arguments, array_args=array_args, array_files=array_files,
                                    appending=appending)
            self.destroy()
            return self.project.subid
        else:
            return None

    def create_project_job(self, project_id=None, project_job_id=None, jobfile_path=None, arrayargsfile_path=None,
                           arguments=None, array_args=None, array_files=None, appending=False):

        if not array_args:
            array_args = []
        if not array_files:
            array_files = []

        if project_id:
            project = self.db_get_project(project_id)
            project.subid = project_job_id
            self.project = project
        # TODO if project is None make sure default project is used

        project_job_exists = False
        if project_job_id:
            project_job_exists = self.db_exists_project_job(project_job_id)

        if not project_job_exists:
            if project_job_id:
                self.log.warn(f"No existing project job for given id {project_job_id:d}")
            if jobfile_path:
                project_job_id = self.db_create_project_job(os.path.basename(jobfile_path))
            else:
                project_job_id = self.db_create_project_job()
        project_job = self.db_get_project_job(project_job_id)

        self.db_update_project_job_appending(project_job_id, appending)

        if project_job.status == JobState.UNASSIGNED.value:
            updated_count = self.db.update(
                f'UPDATE project_jobs SET status={JobState.QUEUED.value:d} WHERE id={project_job.id:d}'
            )
            project_job_init = True if updated_count == 1 else False
        else:
            project_job_init = False
            
        if project_job_init:
            self.log.info(f"Initializing project job {project_job_id:d}")
        else:
            self.log.debug(f"Project job {project_job_id:d} already initialized")

        jobfile_src = None
        if project_job.jobfile:
            if not jobfile_path:
                jobfile_src = self.laravel_base + 'project_job_jobfiles/' + str(self.project.id) + '/' + str(
                    project_job.id) + '/' + project_job.jobfile
            else:
                jobfile_src = jobfile_path

        # array_files = None
        # if project_job.jobfile:
        #     array_files = []
        _array_files = []
        for f in array_files:
            _array_files.append(os.path.basename(f))

        _array_args = []
        if arrayargsfile_path:
            if not arrayargsfile_path and project_job.args:
                args_file_path = self.laravel_base + 'project_job_arguments/' + str(
                    self.project.id) + '/' + str(project_job.id) + '/' + project_job.args
            else:
                args_file_path = arrayargsfile_path
            if args_file_path and os.path.exists(args_file_path):
                with open(args_file_path) as args_file:
                    for arg in args_file.readlines():
                        arg = arg.rstrip()
                        if arg:
                            _array_args.append('"%s"' % arg)
                            # if project_job.jobfile:
                            #     array_files.append(project_job.jobfile)
        if not _array_args:
            _array_args = array_args

        if _array_files and not _array_args:
            for _ in _array_files:
                _array_args.append('""')

        # if self.project.transfer:
        local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
        local_project_job_basedir = os.path.join(local_project_basedir, str(project_job_id))
        local_project_job_in_dir = os.path.join(local_project_job_basedir, self.project.local_input_dir)
        local_project_job_out_dir = os.path.join(local_project_job_basedir, self.project.local_output_dir)
        local_project_job_tmp_dir = os.path.join(local_project_job_basedir, 'tmp')

        if project_job_init or not os.path.exists(local_project_job_basedir):
            if not self.remote:
                if not os.path.exists(local_project_job_basedir):
                    fsutils.makedir(local_project_job_basedir, parents=True, mode=0o775, exist_ok=True)
                if not os.path.exists(local_project_job_in_dir):
                    fsutils.makedir(local_project_job_in_dir, parents=False, mode=0o775)
                if not os.path.exists(local_project_job_out_dir):
                    fsutils.makedir(local_project_job_out_dir, parents=False, mode=0o775)
                if not os.path.exists(local_project_job_tmp_dir):
                    fsutils.makedir(local_project_job_tmp_dir, parents=False, mode=0o775)

        if jobfile_src:
            if not self.remote:
                shutil.copy(jobfile_path, local_project_job_in_dir)
            else:
                # TODO copy per sftp
                shutil.copy(jobfile_path, local_project_job_in_dir)

        self.register_array_job(_array_args, arguments, _array_files)

    def delete_project(self, project_id=None, local_delete=False, remote_delete=True, db_delete=True):
        """Remove project in db and from clusters"""

        if project_id:
            self.project = self.db_get_project(project_id)

        if local_delete:
            local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
            if os.path.exists(local_project_basedir):
                shutil.rmtree(local_project_basedir)

        if remote_delete:
            for cluster in self.project.clusters:
                cluster.connect()

                remote_project_basedir = os.path.join(cluster.basedir, self.project.name)
                cluster.ssh.send_shell('rm -rf %s' % remote_project_basedir)

        if db_delete:
            self.db_delete_project()

    def delete_project_job(self, project_id=None, project_job_id=None,
                           local_delete=False, remote_delete=True, db_delete=True):
        """Remove project in db and from clusters"""

        if project_id:
            self.project = self.db_get_project(project_id)

        # if project_job_id:
        #     project_job = self.db_get_project_job(project_job_id)

        if local_delete:
            local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
            local_project_job_basedir = os.path.join(local_project_basedir, str(project_job_id))
            if os.path.exists(local_project_job_basedir):
                shutil.rmtree(local_project_job_basedir)

        if remote_delete:
            for cluster in self.project.clusters:
                cluster.connect()

                # TODO delete single job dirs
                # remote_project_basedir = os.path.join(cluster.basedir, self.project.name)
                # cluster.ssh.send_shell('rm -rf %s' % remote_project_basedir)

        if db_delete:
            self.db_delete_project_job()

    def restart_failed_job(self, job, cluster=None):
        """ Wrapper for restart_job() """

        self.restart_job(project_id=job.project.id, project_job_id=job.project.subid, job_db_id=job.db_id,
                         local_delete=False, remote_delete=True, cluster=cluster)

    def restart_job(self, project_id=None, project_job_id=None, job_db_id=None, local_delete=True, remote_delete=False,
                    cluster=None):
        """Restart job and delete local transfered results"""

        if project_id:
            self.project = self.db_get_project(project_id)

        if project_job_id:
            self.project.subid = project_job_id

        if job_db_id:
            job = self.db_get_job(job_db_id)
            self.db.update(
                f'UPDATE jobs SET daemon={DaemonState.REGISTERED.value:d}, status={JobState.UNASSIGNED.value:d}, '
                f'restarts=restarts+1 WHERE id={job.db_id:d}')

            if project_job_id:
                total_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i' % project_job_id)[0][0]
                finished_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i AND daemon=%i' % (
                    project_job_id, DaemonState.TRANSFERRED.value))[0][0]
                if total_count == 0:
                    progress = 0
                else:
                    progress = (finished_count / total_count) * 100
                self.db.update(
                    f'UPDATE project_jobs SET status={JobState.RUNNING.value:d}, progress={progress}, '
                    f'finished=0 WHERE id={project_job_id:d}')
            
            if local_delete:
                local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
                local_project_job_basedir = os.path.join(local_project_basedir, str(project_job_id))
                job_dir = job.get_cluster_job_output_id()
                local_job_basedir = os.path.join(local_project_job_basedir,
                                                 f'{self.project.local_output_dir}/{job_dir}')
                if os.path.exists(local_job_basedir):
                    shutil.rmtree(local_job_basedir)

            if remote_delete:
                if not cluster:
                    cluster = job.cluster
                cluster.delete_remote_job_results(self.project, job.get_cluster_job_output_id())
        else:
            self.log.warn(f'Job not restarted - missing job id.')

    def restart_array_job(self, project_id=None, project_job_id=None, arrayjob_id=None,
                          local_delete=True, remote_delete=False, cluster=None):
        """Restart array_job and delete local transfered results"""

        if project_id:
            self.project = self.db_get_project(project_id)

        if project_job_id:
            self.project.subid = project_job_id

        if arrayjob_id:
            if local_delete:
                # local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
                # local_project_job_basedir = os.path.join(local_project_basedir, str(project_job_id))
                # TODO implement
                self.log.warn(" --- NOT IMPLEMENTED ---")
                pass

            if remote_delete:
                if not cluster:
                    self.log.warn(f'Arrayjob not restarted - no cluster specified.')
                else:
                    cluster.delete_remote_array_job_results(self.project, arrayjob_id)

            self.db.update(
                f'UPDATE jobs SET daemon={DaemonState.REGISTERED.value:d}, status={JobState.UNASSIGNED.value:d}, '
                f'restarts=restarts+1 WHERE jobid={arrayjob_id:d}')

            if project_job_id:
                total_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i' % project_job_id)[0][0]
                finished_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i AND daemon=%i' % (
                    project_job_id, DaemonState.TRANSFERRED.value))[0][0]
                if total_count == 0:
                    progress = 0
                else:
                    progress = (finished_count / total_count) * 100
                self.db.update(
                    f'UPDATE project_jobs SET status={JobState.RUNNING.value:d}, progress={progress}, '
                    f'finished=0 WHERE id={project_job_id:d}')
        else:
            self.log.warn(f'Arrayjob not restarted - missing arrayjob id.')

    def restart_single_job(self, project_id=None, project_job_id=None, singlejob_id=None,
                           local_delete=True, remote_delete=False, cluster=None):
        """Restart array_job and delete local transfered results"""

        if project_id:
            self.project = self.db_get_project(project_id)

        if project_job_id:
            self.project.subid = project_job_id

        if singlejob_id:
            if local_delete:
                # local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
                # local_project_job_basedir = os.path.join(local_project_basedir, str(project_job_id))
                # TODO implement
                self.log.warn(" --- NOT IMPLEMENTED ---")
                pass

            if remote_delete:
                if not cluster:
                    self.log.warn(f'Arrayjob not restarted - no cluster specified.')
                else:
                    cluster.delete_remote_job_results(self.project, singlejob_id)

            self.db.update(
                f'UPDATE jobs SET daemon={DaemonState.REGISTERED.value:d}, status={JobState.UNASSIGNED.value:d}, '
                f'restarts=restarts+1 WHERE jobid={singlejob_id:d}')

            if project_job_id:
                total_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i' % project_job_id)[0][0]
                finished_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i AND daemon=%i' % (
                    project_job_id, DaemonState.TRANSFERRED.value))[0][0]
                if total_count == 0:
                    progress = 0
                else:
                    progress = (finished_count / total_count) * 100
                self.db.update(
                    f'UPDATE project_jobs SET status={JobState.RUNNING.value:d}, progress={progress}, '
                    f'finished=0 WHERE id={project_job_id:d}')
        else:
            self.log.warn(f'Singlejob not restarted - missing singlejob id.')

    def db_update_array_job_deamon_status(self, jobs, status):
        """Update array job status in database"""

        query = 'INSERT INTO jobs (id, cmd, daemon, created) VALUES '
        for job in jobs[:-1]:
            query += '(%i, "", %i, NOW()),' % (job.db_id, status)
        query += '(%i, "", %i, NOW()) ' % (jobs[-1].db_id, status)
        query += 'ON DUPLICATE KEY UPDATE daemon=VALUES(daemon);'
        self.db.update_on_duplicate_key(query)

    def db_update_array_job_submitted(self, jobs):
        """Update array job in database: add submission details"""

        query = 'INSERT INTO jobs (id, cmd, daemon, status, cluster, jobid, arrayidx, submit, created) VALUES '
        for job in jobs[:-1]:
            query += f'({job.db_id:d}, "", {job.daemon:d}, {job.status:d}, "{job.cluster}", "{job.job_id}", ' \
                     f'{job.job_array_idx:d}, "{job.submit}", NOW()),'
        query += f'({jobs[-1].db_id:d}, "", {jobs[-1].daemon:d}, {jobs[-1].status:d}, "{jobs[-1].cluster}", ' \
                 f'"{jobs[-1].job_id}", {jobs[-1].job_array_idx:d}, "{jobs[-1].submit}", NOW()) '
        query += 'ON DUPLICATE KEY UPDATE daemon=VALUES(daemon), status=VALUES(status), cluster=VALUES(cluster), ' \
                 'jobid=VALUES(jobid), arrayidx=VALUES(arrayidx), submit=VALUES(submit); '

        self.db.update_on_duplicate_key(query)

    def db_update_job_submitted(self, job):
        """Update job in database: add submission details"""

        self.db.update('UPDATE jobs SET daemon=%i, status=%i, cluster="%s", jobid=%i, submit="%s" WHERE id=%i;' % (
            job.daemon, job.status, job.cluster, job.job_id, job.submit, job.db_id))

    def db_update_jobs_submitted(self, jobs):
        """Update multiple jobs in database: add submission details"""

        query = 'INSERT INTO jobs (id, cmd, daemon, status, cluster, jobid, submit) VALUES '
        for job in jobs[:-1]:
            query += f'({job.db_id:d}, "", {job.daemon:d}, {job.status:d}, "{job.cluster}", "{job.job_id}", ' \
                     f'"{job.submit}"),'
        query += f'({jobs[-1].db_id:d}, "", {jobs[-1].daemon:d}, {jobs[-1].status:d}, "{jobs[-1].cluster}", ' \
                 f'"{jobs[-1].job_id}", "{jobs[-1].submit}") '
        query += 'ON DUPLICATE KEY UPDATE daemon=VALUES(daemon), status=VALUES(status), cluster=VALUES(cluster), ' \
                 'jobid=VALUES(jobid), submit=VALUES(submit); '
        self.db.update_on_duplicate_key(query)

    def db_update_job_status(self, job):
        """Update job in database: update submission status"""

        self.db.update('UPDATE jobs SET status=%i WHERE id=%i;' % (job.status, job.db_id))

    def db_update_job_daemon_status(self, job_id, status):
        """Update job status in database manual"""

        self.db.update('UPDATE jobs SET daemon=%i WHERE id=%i;' % (status, job_id))

    def db_update_jobs_statuses(self, jobs):
        """Update job in database: update submission status"""

        timestamp = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

        query = 'INSERT INTO jobs (id, cmd, daemon, status, start, end, created) VALUES '
        for job in jobs:
            if job.status == JobState.RUNNING.value:
                query += '(%i, "", %i, %i, "%s", end, NOW())' % (job.db_id, job.daemon, job.status, timestamp)
            elif job.status == JobState.FINISHED.value:
                if job.start:
                    query += f'({job.db_id:d}, "", {job.daemon:d}, {job.status:d}, "{job.start}", "{job.end}", NOW())'
                else:
                    query += f'({job.db_id:d}, "", {job.daemon:d}, {job.status:d}, submit, "{timestamp}", NOW())'
            elif job.status == JobState.FAILED.value:
                query += f'({job.db_id:d}, "", {job.daemon:d}, {job.status:d}, "{timestamp}", end, NOW())'
            if job.db_id != int(jobs[-1].db_id):
                query += ','
        # TODO remove
        # previous ##################################
        # if job.status == JobState.RUNNING.value:
        #     query += 'ON DUPLICATE KEY UPDATE daemon=VALUES(daemon), status=VALUES(status), start=IF(start IS NULL, '\
        #              'VALUES(start), start), end=VALUES(end); '
        # elif job.status == JobState.FINISHED.value:
        #     query += 'ON DUPLICATE KEY UPDATE daemon=VALUES(daemon), status=VALUES(status), start=IF(start IS NULL, '\
        #              'submit, start), end=VALUES(end); '
        #############################################
        query += f' ON DUPLICATE KEY UPDATE daemon=VALUES(daemon), status=VALUES(status), ' \
                 f'start=CONVERT_TZ(IF(start IS NULL or VALUES(status) = ' \
                 f'{JobState.FINISHED.value:d}, VALUES(start), start),"{self.db.timezone}","{self.timezone}"), ' \
                 f'end=CONVERT_TZ(VALUES(end),"{self.db.timezone}","{self.timezone}");'
        self.db.update_on_duplicate_key(query)

    def db_update_project_job_status(self):
        """Update project job status"""

        running_project_jobs = self.db.query(
            f'SELECT id, submitted, started, status, progress, appending FROM project_jobs '
            f'WHERE project={self.project.id:d} '
            f'AND status < {JobState.FINISHED.value:d}')
        query = 'INSERT INTO project_jobs (id, status, progress, submitted, started, finished, created_at) VALUES '
        query_values = []
        execute_query = False
        projobs_details = {}
        for projob in running_project_jobs:
            projob_id = int(projob[0])
            projob_submitted = None if projob[1] == '0000-00-00 00:00:00' else projob[1]
            projob_started = None if projob[2] == '0000-00-00 00:00:00' else projob[2]
            status = int(projob[3])
            progress_db = int(projob[4])
            appending = int(projob[5])
            total_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i' % projob_id)[0][0]
            finished_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i AND daemon=%i' % (
                projob_id, DaemonState.TRANSFERRED.value))[0][0]
            failed_count = self.db.query('SELECT COUNT(*) FROM jobs WHERE project_job=%i AND status=%i' % (
                projob_id, JobState.FAILED.value))[0][0]
            if total_count == 0:
                progress = 0
            else:
                progress = (finished_count / total_count) * 100

            stats_query = None
            if appending or not status:
                pass
            elif not projob_submitted or not projob_started:
                stats_query = f'(SELECT MIN(submit), MIN(start) FROM jobs WHERE project_job={projob_id:d})'

            update_progress_only = False
            if appending:
                if progress > progress_db:
                    update_progress_only = True
            else:
                if progress < 100:
                    if total_count > 0 and total_count == (failed_count + finished_count):
                        status = JobState.FAILED.value
                        stats_query = f'(SELECT MIN(submit), MIN(start), MAX(end) ' \
                                      f'FROM jobs WHERE project_job={projob_id:d})'
                        # TODO how to proceed from here, if job failed (either more than 3 retries or transfers)
                        # TODO in case of no transfer the daemon state is also set to DaemonState.FAILED.value
                        self.log.warn(f' + {"-":10} > {self.project.name:15} > {projob_id:7d} > '
                                      f'finished with {failed_count} jobs in failed state')
                    else:
                        status = JobState.RUNNING.value
                        if progress > progress_db:
                            update_progress_only = True
                else:
                    status = JobState.FINISHED.value
                    stats_query = f'(SELECT MIN(submit), MIN(start), MAX(end) FROM jobs ' \
                                  f'WHERE project_job={projob_id:d})'

            projobs_details[projob_id] = (status, progress)

            if stats_query:
                execute_query = True
                res = self.db.query(stats_query)[0]
                if len(res) == 3:
                    submitted, started, finished = res
                    submitted = '0000-00-00 00:00:00' if not submitted else submitted
                    started = '0000-00-00 00:00:00' if not started else started
                    finished = '0000-00-00 00:00:00' if not finished else finished
                    query_values.append(f'({projob_id:d}, {status:d}, {progress:f}, '
                                        f'"{submitted}", "{started}", "{finished}", NOW())')
                else:
                    submitted, started = res
                    submitted = '0000-00-00 00:00:00' if not submitted else submitted
                    started = '0000-00-00 00:00:00' if not started else started
                    query_values.append(f'({projob_id:d}, {status:d}, {progress:f}, '
                                        f'"{submitted}", "{started}", 0, NOW())')
            elif update_progress_only:
                    submitted = '0000-00-00 00:00:00' if not projob_submitted else projob_submitted
                    started = '0000-00-00 00:00:00' if not projob_started else projob_started
                    query_values.append(f'({projob_id:d}, {status:d}, {progress:f}, '
                                        f'"{submitted}", "{started}", 0, NOW())')
        query += ','.join(query_values)
        query += ' ON DUPLICATE KEY UPDATE status=VALUES(status), progress=VALUES(progress), ' \
                 'submitted=VALUES(submitted), started=VALUES(started), finished=VALUES(finished);'

        if execute_query:
            self.db.update_on_duplicate_key(query)

        # TODO mark failed jobs

        return projobs_details

    def db_update_project_job_appending(self, jobid, appending):
        """Update project job appending status"""

        self.db.update(f'UPDATE project_jobs SET appending={1 if appending else 0:d} WHERE id={jobid:d}')

    def db_create_project(self):
        """Add new project to db"""

        project_clusters = 'NULL'
        if self.project.clusters:
            _cluster_list = []
            for cluster in self.project.clusters:
                _cluster_list.append(cluster.name)
            project_clusters = ",".join(_cluster_list)

        if self.project.user:
            project_user = '"%s"' % self.project.user
        else:
            project_user = 'NULL'

        self.project.id = self.db.insert(
            f'INSERT INTO projects (name, description, user, script, valid_clusters, transfer_dir, created_at) '
            f'VALUES("{self.project.name}", "{self.project.description}", {project_user}, "{self.project.script}", '
            f'"{project_clusters}", "{self.project.remote_result_transfer_dir}", NOW());'
        )

        return self.project.id

    def db_delete_project(self):
        """Delete project from db"""

        self.db.update('DELETE FROM jobs WHERE project=%i' % self.project.id)
        self.db.update('DELETE FROM project_jobs WHERE project=%i' % self.project.id)
        self.db.update('DELETE FROM projects WHERE id=%i' % self.project.id)

        return

    def db_delete_project_job(self):
        """Delete project job from db"""

        self.db.update('DELETE FROM jobs WHERE project_job=%i' % self.project.subid)
        self.db.update('DELETE FROM project_jobs WHERE id=%i' % self.project.subid)

        return

    def db_create_project_job(self, file=None):
        """Add new project job to database; returns new project job id"""

        project_job_file = 'NULL'
        if file:
            project_job_file = '"%s"' % file

        projectid = self.db.insert(
            'INSERT INTO project_jobs (project, jobfile, priority, registered, created_at) '
            'VALUES(%i, "%s", %i, NOW(), NOW());' % (self.project.id, project_job_file, self.priority)
            )
        self.project.subid = projectid
        return projectid

    def db_get_project_id(self, project_name):
        """Get project id from db"""

        res = self.db.query(f'SELECT id FROM projects WHERE projects.name="{project_name}";')[0][0]
        return res

    def db_get_project(self, projectid):
        """Get project from db"""

        res = self.db.query(
            f'SELECT id, name, description, script, sources, valid_clusters, transfer_dir, pre_processor, '
            f'post_processor, dbstore FROM projects WHERE projects.id={projectid:d};')[0]

        available_clusters = Cluster.get_enabled_clusters_from_config()
        if res[5] is not None:
            valid_clusters = []
            for cluster_name in res[5].split(','):
                valid_clusters.append(Cluster(cluster_name))
        else:
            valid_clusters = available_clusters
        project = Project.create_registered_full(int(res[0]), res[1], res[2], res[3], res[4],
                                                 valid_clusters, res[6], res[7], res[8], bool(int(res[9])))
        return project

    def db_exists_project_job(self, project_job_id):
        """Tests if project jobs exists in db"""

        return self.db.query(f'SELECT COUNT(*) FROM project_jobs WHERE id={project_job_id:d};')[0]

    def db_get_project_job(self, project_job_id):
        """Get active project jobs from db"""

        res = self.db.query(
            f'SELECT project, status, progress, jobfile, args FROM project_jobs WHERE id={project_job_id:d};')[0]

        project_job = Project_Job(
            project_job_id, int(res[0]), int(res[1]), int(res[2]), None if res[3] == "NULL" else res[3], res[4]
        )
        return project_job

    def db_get_project_job_array_jobs_ids(self, project_job_id):
        """Get array_job ids for project_job from db"""

        array_jobs_ids = []
        resultset = self.db.query(
            f'SELECT DISTINCT(jobid) FROM jobs WHERE project_job={project_job_id:d} AND NOT ISNULL(arrayidx);')
        for res in resultset:
            array_jobid = res[0]
            array_jobs_ids.append(array_jobid)

        return array_jobs_ids

    def db_get_project_job_single_jobs_ids(self, project_job_id):
        """Get single_job ids for project_job from db"""

        single_jobs_ids = []
        resultset = self.db.query(
            f'SELECT jobid FROM jobs WHERE project_job={project_job_id:d} AND ISNULL(arrayidx);')
        for res in resultset:
            single_jobid = res[0]
            single_jobs_ids.append(single_jobid)

        return single_jobs_ids

    def db_get_job(self, db_id):
        """Get job from db"""

        res = self.db.query(
            f'SELECT id, jobid, arrayidx, status, cluster, restarts FROM jobs WHERE id={db_id:d};')[0]

        if res[4] and res[4] != 'NULL':
            cluster = Cluster(res[4])
        else:
            cluster = None

        job = Job.create_submitted_minimal(int(res[0]), cluster, int(res[1]), int(res[2]), int(res[3]), int(res[5]))
        return job

    def db_get_active_jobs_count(self):
        """"Get number of not finished jobs for project"""

        project = ''
        if self.project is not None:
            project = 'AND project=%i' % self.project.id
        job_count = self.db.query(f'SELECT COUNT(*) FROM jobs WHERE daemon BETWEEN '
                                  f'{DaemonState.SUBMITTED.value:d} AND {DaemonState.VALIDATED.value:d} '
                                  f'{project};')[0][0]
        return job_count

    def db_get_registered_jobs(self, limit=0):
        """Get registered jobs from database, optionally limited by count"""

        limited = ''
        if limit > 0:
            limited = 'limit %i' % limit
        project = ''
        if self.project is not None:
            project = 'AND projects.name="%s"' % self.project.name
        resultset = self.db.query(
            f'SELECT jobs.id, projects.name, project_jobs.project, project_job, project_jobs.priority, cmd, file, restarts '
            f'FROM jobs, project_jobs, projects WHERE jobs.project = projects.id AND jobs.project_job = project_jobs.id '
            f'AND daemon={DaemonState.REGISTERED.value:d} {project} ORDER BY project_jobs.priority DESC, project_job {limited};'
        )
        jobs = []
        for res in resultset:
            registered_job = Job.create_registered(int(res[0]), res[1], int(res[2]), int(res[3]),
                                                   res[5], res[6], int(res[7]))
            registered_job.project.remote_result_transfer_dir = self.project.remote_result_transfer_dir
            jobs += [registered_job]
        return jobs

    def db_get_submitted_jobs_for_cluster(self, cluster):
        """Get submitted jobs from database"""

        project = ''
        if self.project is not None:
            project = 'AND projects.name="%s"' % self.project.name
        resultset = self.db.query(
            f'SELECT jobs.id, jobid, arrayidx, status, restarts, project, projects.name, project_job FROM '
            f'jobs, projects WHERE jobs.project = projects.id AND cluster="{cluster.name}" {project} AND '
            f'daemon={DaemonState.SUBMITTED.value:d} AND status BETWEEN {JobState.QUEUED.value:d} AND '
            f'{JobState.RUNNING.value:d}'
        )
        jobs = []
        for res in resultset:
            registered_job = Job.create_submitted_minimal(int(res[0]), cluster,
                                                          int(res[1]), int(res[2]), int(res[3]), int(res[4]))
            registered_job.project = Project(res[6], projectid=int(res[5]), subid=int(res[7]))
            jobs.append(registered_job)
        return jobs

    def db_get_validated_jobs_for_cluster(self, cluster):
        """Get validated jobs from database"""

        project = ''
        if self.project is not None:
            project = 'AND projects.name="%s"' % self.project.name
        resultset = self.db.query(
            f'SELECT jobs.id, jobid, arrayidx, status, restarts, project, projects.name, project_job '
            f'FROM jobs, projects WHERE jobs.project = projects.id AND cluster="{cluster.name}" {project} '
            f'AND daemon={DaemonState.VALIDATED.value:d}'
        )
        jobs = []
        for res in resultset:
            registered_job = Job.create_submitted_minimal(int(res[0]), cluster,
                                                          int(res[1]), int(res[2]), int(res[3]), int(res[4]))
            registered_job.project = Project(res[6], projectid=int(res[5]), subid=int(res[7]))
            jobs.append(registered_job)
        return jobs

    def submit_job(self, job, cluster=None):
        """Submit single job; job is updated with cluster jobid and other submission details"""

        self.submit_jobs([job], cluster)

    def submit_jobs(self, jobs, cluster=None):
        """Submit multiple jobs; jobs are updated with cluster jobid and other submission details"""

        if cluster is None:
            cluster = self.cluster_list[0]

        cluster.submit_jobs(jobs)

        if self.db is not None:
            self.db_update_jobs_submitted(jobs)

        job_count = len(jobs)
        if job_count == 1:
            if jobs[0].db_id is not None:
                self.log.info('Submitted job id: %i db: %i' % (jobs[0].job_id, jobs[0].db_id))
            else:
                self.log.info('Submitted job id: %i' % jobs[0].job_id)
        else:
            if jobs[0].db_id is not None:
                self.log.info('Submitted %i jobs ids: %i-%i db: %i-%i' %
                              (job_count, jobs[0].job_id, jobs[-1].job_id, jobs[0].db_id, jobs[-1].db_id))
            else:
                self.log.info('Submitted %i jobs ids: %i-%i' % (job_count, jobs[0].job_id, jobs[-1].job_id))

    def submit_array_job(self, jobs, cluster=None):
        """Submit jobs as array job; jobs are updated with cluster jobid and other submission details"""

        if cluster is None:
            cluster = self.cluster_list[0]

        cluster.submit_array_job(jobs)

        if self.db is not None:
            self.db_update_array_job_submitted(jobs)
        self.log.debug(
            f' + {cluster.name:10} > submitted array job id: {jobs[0].job_id:d} '
            f'db: {jobs[0].db_id:d}-{jobs[-1].db_id:d}'
        )

    def create_job(self):
        # TODO implement create_job
        pass

    def create_jobs(self):
        # TODO implement create_jobs
        pass

    def create_array_job(self):
        # TODO implement create_array_job
        pass

    def register_array_job(self, array_args, arguments=None, array_files=None):
        """Register jobs in database"""

        if arguments:
            _cmd = ' args="%s"' % arguments
        else:
            _cmd = ''

        sql_query = 'INSERT INTO jobs (project, project_job, created, cmd, file) VALUES '
        for idx, array_arg in enumerate(array_args[:-1]):
            _file = 'NULL'
            if array_files:
                _file = '"%s"' % array_files[idx]
            cmd = '\'array=%s' % array_arg + _cmd + '\''
            sql_query += '(%i, %i, NOW(), %s, %s),' \
                         % (self.project.id, self.project.subid, cmd, _file)
        _file = 'NULL'
        if array_files:
            _file = '"%s"' % array_files[-1]
        cmd = '\'array=%s' % array_args[-1] + _cmd + '\''
        sql_query += '(%i, %i, NOW(), %s, %s);' \
                     % (self.project.id, self.project.subid, cmd, _file)

        return self.db.insert(sql_query)

    def get_cluster_job_states(self, cluster=None):
        """Check current jobs at clusters"""

        cluster_job_lists = {}
        cluster_list = self.cluster_list
        if cluster is not None:
            cluster_list = [cluster]

        for selected_cluster in cluster_list:
            job_list = selected_cluster.get_state_counts()
            cluster_job_lists[selected_cluster.name] = job_list

        if cluster is not None:
            cluster_job_lists = cluster_job_lists[cluster.name]

        return cluster_job_lists

    def db_lock_jobs_for_transfer(self, cluster_name, limit_single=None, limit_array=None):
        max_arrayjob_transfers = limit_array if limit_array else int(self.config.get_property('clubber', 't.arrayjob.transfers'))
        max_singlejob_transfers = limit_single if limit_single else int(self.config.get_property('clubber', 't.singlejob.transfers'))

        project = ''
        if self.project is not None:
            project = 'AND projects.name="%s"' % self.project.name
        resultset = self.db.query(
            f'SELECT jobs.id, jobid, project_job, project_jobs.priority, COUNT(*) as jobcount, jobs.status, arrayidx '
            f'FROM jobs, project_jobs, projects WHERE jobs.project = projects.id AND jobs.project_job = project_jobs.id '
            f'AND cluster="{cluster_name}" {project} AND daemon BETWEEN {DaemonState.SUBMITTED.value:d} AND '
            f'{DaemonState.VALIDATED.value:d} GROUP BY jobid, status ORDER BY '
            f'project_jobs.priority DESC, project_job, jobs.status;'
        )

        unfinished_jobs = []
        arrayjobs = []
        singlejobs = []
        for job in resultset:
            _primarykey = int(job[0])
            _jobid = int(job[1])
            _project_job = int(job[2])
            _project_job_priority = int(job[3])
            _jobcount = int(job[4])
            _status = int(job[5])
            _arrayidx = job[6]

            if _status != JobState.FINISHED.value:
                unfinished_jobs.append(_jobid)
            elif _jobid not in unfinished_jobs:
                if _jobcount == 1 and _arrayidx is None:
                    singlejobs.append((_jobid, _project_job, _primarykey))
                else:
                    arrayjobs.append((_jobid, _project_job, _primarykey))

        arrayjobs_subset = []
        for arrayjob in arrayjobs[:max_arrayjob_transfers]:
            arrayjobid = arrayjob[0]
            self.db.update(f'UPDATE jobs SET daemon={DaemonState.TRANSFERRING.value:d} WHERE jobid={arrayjobid:d}')
            arrayjobs_subset.append(arrayjob)

        singlejobs_subset = []
        for singlejob in singlejobs[:max_singlejob_transfers]:
            singlejobid = singlejob[0]
            self.db.update(f'UPDATE jobs SET daemon={DaemonState.TRANSFERRING.value:d} WHERE jobid={singlejobid:d}')
            singlejobs_subset.append(singlejob)

        return arrayjobs_subset, singlejobs_subset, unfinished_jobs

    def db_unlock_jobs_in_validation(self, arrayjobs, singlejobs):

        for arrayjob in arrayjobs:
            arrayjobid = arrayjob[0]
            self.db.update(f'UPDATE jobs SET daemon={DaemonState.VALIDATED.value:d} WHERE jobid={arrayjobid:d}')

        for singlejob in singlejobs:
            singlejobid = singlejob[0]
            self.db.update(f'UPDATE jobs SET daemon={DaemonState.VALIDATED.value:d} WHERE jobid={singlejobid:d}')

    def db_unlock_jobs_in_transfer(self, arrayjobs, singlejobs):
        for arrayjob in arrayjobs:
            arrayjobid = arrayjob[0]
            self.db.update(f'UPDATE jobs SET daemon={DaemonState.VALIDATED.value:d} WHERE jobid={arrayjobid:d} AND '
                           f'daemon!={DaemonState.TRANSFERRED.value:d}')

        for singlejob in singlejobs:
            singlejobid = singlejob[0]
            self.db.update(f'UPDATE jobs SET daemon={DaemonState.VALIDATED.value:d} WHERE jobid={singlejobid:d} AND '
                           f'daemon!={DaemonState.TRANSFERRED.value:d}')

    def transfer_job_results(self, cluster_name, arrayjobs, singlejobs):
        """Retrieve results from cluster"""

        self.log.debug(f' + {cluster_name:10} > {self.project.name:15} > retrieving jobs...')
        cluster = Cluster(cluster_name)

        # noinspection PyBroadException
        try:
            cluster.connect()
            self.db.connect()
            
            resultfiles_list = []

            if not self.project.local_working_dir:
                self.log.error(f' + {cluster.name:10} > {self.project.name:15} > '
                            f'no local working directory defined for project - skipping transfer!')
            else:
                # --- array jobs ---
                for arrayjob in arrayjobs:
                    arrayjobid = arrayjob[0]
                    project_job = arrayjob[1]
                    self.project.subid = project_job
                    # project_subid_base = os.path.join(self.project.local_working_dir, str(project_job))
                    project_subid_base = os.path.join(self.project.local_working_dir,
                                                    "%s/%i" % (self.project.name, project_job))
                    # project_subid_outdir = os.path.join(project_subid_base, self.project.local_output_dir)
                    project_subid_outdir = os.path.join(project_subid_base, self.project.local_output_dir)
                    self.log.info(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                f'retrieving arrayjob {arrayjobid}...')
                    local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
                    local_project_job_basedir = os.path.join(local_project_basedir, str(self.project.subid))
                    local_project_job_out_dir = os.path.join(local_project_job_basedir, self.project.local_output_dir)
                    local_project_job_tmp_dir = os.path.join(local_project_job_basedir, 'tmp')
                    if not os.path.exists(local_project_job_basedir):
                        fsutils.makedir(local_project_job_basedir, parents=True, mode=0o775, exist_ok=True)
                    if not os.path.exists(local_project_job_out_dir):
                        fsutils.makedir(local_project_job_out_dir, parents=False, mode=0o775)
                    if not os.path.exists(local_project_job_tmp_dir):
                        fsutils.makedir(local_project_job_tmp_dir, parents=False, mode=0o775)
                    compressed, extracted = None, None
                    # noinspection PyBroadException
                    try:
                        compressed, extracted = cluster.retrieve_array_job_results(
                            self.project, arrayjobid, project_subid_outdir
                        )
                    except Exception as err:
                        self.log.error(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                    f'failed to retrieve arrayjob {arrayjobid}: {err}')
                    if extracted is None:
                        max_restarts = self.db.query(f'SELECT MAX(restarts) FROM jobs WHERE jobid={arrayjobid:d};')[0][0]
                        if max_restarts < int(self.config.get_property('clubber', 'max.job.restarts')):
                            self.restart_array_job(project_id=self.project.id, project_job_id=self.project.subid,
                                                arrayjob_id=arrayjobid, local_delete=False, remote_delete=True,
                                                cluster=cluster)
                            self.log.warn(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                        f'restarted arrayjob {arrayjobid}')
                        else:
                            self.db.update(f'UPDATE jobs SET daemon={DaemonState.FAILED.value:d} WHERE jobid={arrayjobid:d}')
                            self.log.warn(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                        f'no results to retrieve for arrayjob {arrayjobid} - marked as FAILED')
                    else:
                        self.log.debug(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                    f'done: retrieving arrayjob {arrayjobid}')
                        if self.project.dbstore:
                            compressed_filesize_mb = os.path.getsize(compressed) / 1000000
                            with open(compressed, 'rb') as f:
                                data = f.read()
                            compressed_basename = os.path.basename(compressed)
                            compressed_filename, compressed_extension = os.path.splitext(compressed_basename)
                            query = 'INSERT INTO results (project, project_job, job, bin_data, ' \
                                    'txt_data, filename, filesize, filetype, created_at) VALUES ' \
                                    '(%s, %s, %s, %s, %s, %s, %s, %s, NOW());'
                            arguments = (self.project.id, project_job, None, data, None,
                                        compressed_filename, compressed_filesize_mb, compressed_extension)
                            self.db.insert(query, arguments)
                            os.remove(compressed)

                        # TODO deprecated - see line 1363
                        # for root, dirs, files in os.walk(project_subid_outdir):
                        #     for d in dirs:
                        #         os.chmod(os.path.join(root, d), 0o777)
                        #     for f in files:
                        #         os.chmod(os.path.join(root, f), 0o777)

                        for job_folder in next(os.walk(project_subid_outdir))[1]:
                            # skip hidden folders
                            if job_folder[0] == '.':
                                continue
                            job_folder_path = os.path.join(project_subid_outdir, job_folder)
                            resultfiles_list.append(job_folder_path)

                        self.db.update(f'UPDATE jobs SET daemon={DaemonState.TRANSFERRED.value:d} WHERE jobid={arrayjobid:d}')

                # --- single jobs ---
                for singlejob in singlejobs:
                    singlejobid = singlejob[0]
                    project_job = singlejob[1]
                    single_primary_key = singlejob[2]
                    self.project.subid = project_job
                    # project_subid_base = os.path.join(self.project.local_working_dir, str(project_job))
                    project_subid_base = os.path.join(self.project.local_working_dir,
                                                    "%s/%i" % (self.project.name, project_job))
                    # project_subid_outdir = os.path.join(project_subid_base, self.project.local_output_dir)
                    project_subid_outdir = os.path.join(project_subid_base, self.project.local_output_dir)
                    self.log.info(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                f'retrieving job {singlejobid}...')
                    local_project_basedir = os.path.join(self.project.local_working_dir, self.project.name)
                    local_project_job_basedir = os.path.join(local_project_basedir, str(self.project.subid))
                    local_project_job_out_dir = os.path.join(local_project_job_basedir, self.project.local_output_dir)
                    local_project_job_tmp_dir = os.path.join(local_project_job_basedir, 'tmp')
                    if not os.path.exists(local_project_job_basedir):
                        fsutils.makedir(local_project_job_basedir, parents=True, mode=0o775, exist_ok=True)
                    if not os.path.exists(local_project_job_out_dir):
                        fsutils.makedir(local_project_job_out_dir, parents=False, mode=0o775)
                    if not os.path.exists(local_project_job_tmp_dir):
                        fsutils.makedir(local_project_job_tmp_dir, parents=False, mode=0o775)
                    compressed, extracted = None, None
                    # noinspection PyBroadException
                    try:
                        compressed, extracted = cluster.retrieve_job_results(
                            self.project, singlejobid, project_subid_outdir
                        )
                    except Exception as err:
                        self.log.error(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                    f'failed to retrieve job {singlejobid}: {err}')
                    if extracted is None:
                        restarts = self.db.query(f'SELECT restarts FROM jobs WHERE jobid={singlejobid:d};')[0][0]
                        if restarts < int(self.config.get_property('clubber', 'max.job.restarts')):
                            self.restart_single_job(project_id=self.project.id, project_job_id=self.project.subid,
                                                    singlejob_id=singlejobid, local_delete=False, remote_delete=True,
                                                    cluster=cluster)
                            self.log.warn(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                        f'restarted singlejob {singlejobid}')
                        else:
                            self.db.update(f'UPDATE jobs SET daemon={DaemonState.FAILED.value:d} WHERE jobid={singlejobid:d}')
                            self.log.warn(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                        f'no results to retrieve for singlejob {singlejobid} - marked as FAILED')
                    else:
                        self.log.debug(f' + {cluster.name:10} > {self.project.name:15} > {project_job:7d} > '
                                    f'done: retrieving job {singlejobid}')
                        if self.project.dbstore:
                            compressed_filesize_mb = os.path.getsize(compressed) / 1000000
                            with open(compressed, 'rb') as f:
                                data = f.read()
                            compressed_basename = os.path.basename(compressed)
                            compressed_filename, compressed_extension = os.path.splitext(compressed_basename)
                            query = f'INSERT INTO results (project, project_job, job, bin_data, txt_data, filename, ' \
                                    f'filesize, filetype, created_at) VALUES ({self.project.id:d}, {project_job:d}, ' \
                                    f'{single_primary_key:d}, "{data}", "{None}", "{compressed_filename}", ' \
                                    f'"{compressed_filesize_mb}", "{compressed_extension}", NOW()); '
                            self.db.insert(query)
                            os.remove(compressed)

                        # TODO deprecated - see line 1363
                        # for root, dirs, files in os.walk(project_subid_outdir):
                        #     for d in dirs:
                        #         os.chmod(os.path.join(root, d), 0o777)
                        #     for f in files:
                        #         os.chmod(os.path.join(root, f), 0o777)

                        for job_folder in next(os.walk(project_subid_outdir))[1]:
                            # skip hidden folders
                            if job_folder[0] == '.':
                                continue
                            job_folder_path = os.path.join(project_subid_outdir, job_folder)
                            resultfiles_list.append(job_folder_path)

                        self.db.update(f'UPDATE jobs SET daemon={DaemonState.TRANSFERRED.value:d} WHERE jobid={singlejobid:d}')

        except Exception as err:
            self.log.error(f' {cluster_name:10} > {self.project.name:15} > EXCEPTION in threaded transfer: {err}')
            self.db_unlock_jobs_in_transfer(arrayjobs, singlejobs)
        finally:
            self.db.disconnect()
            cluster.disconnect()
        
        self.log.debug(f' + {cluster_name:10} > {self.project.name:15} > done: retrieving jobs')     
        return resultfiles_list

    def retrieve_job_results_from_db(self, outfolder, project_id, project_job_id, job_key_id=None):
        job_query = ''
        if job_key_id:
            job_query = f'AND job={job_key_id:d}'
        query = f'SELECT bin_data, txt_data, filename, filesize, filetype, created_at FROM results WHERE ' \
                f'project={project_id:d} AND project_job={project_job_id:d} {job_query}'
        bin_data, txt_data, filename, filesize, filetype, created_at = self.db.query(query)[0]
        save_as = os.path.join(outfolder, filename + filetype)
        with open(save_as, 'wb') as f:
            f.write(bin_data)
        return save_as

    def validate_and_transfer(self, cluster_name):
        cluster = Cluster(cluster_name)
        db_arrayjobs_all, db_singlejobs_all = ([], [])
        db_arrayjobs, db_singlejobs, cluster_finished_ok = ([], [], [])
        # noinspection PyBroadException
        try:
            self.log.debug(
                f' + {cluster_name:10} > {self.project.name:15} > '
                f'acquiring thread lock for job validation and transfer...'
            )
            if self.threadlock:
                self.threadlock.acquire()
            cluster.connect()
            self.db.connect()
            self.log.debug(f' + {cluster_name:10} > {self.project.name:15} > validating jobs...')
            active_jobs = self.db_get_submitted_jobs_for_cluster(cluster)
            cluster_running, cluster_finished_ok, cluster_finished_failed = self.validate_jobs(cluster, active_jobs)
            self.log.debug(f' + {cluster_name:10} > {self.project.name:15} > done: validating jobs')
            if self.project.transfer:
                self.log.debug(f' + {cluster_name:10} > {self.project.name:15} > starting transfer...')
                max_slots = self.transfer_pool._max_workers
                free_slots = max_slots - self.transfer_pool._work_queue.qsize()
                keep_queuing = True
                while free_slots > 0 and keep_queuing:
                    db_arrayjobs, db_singlejobs, db_unfinished = self.db_lock_jobs_for_transfer(cluster_name)
                    db_arrayjobs_all.extend(db_arrayjobs)
                    db_singlejobs_all.extend(db_singlejobs)
                    if db_singlejobs or db_arrayjobs:
                        self.transfer_pool.submit(
                            self.duplicate().transfer_job_results,
                            cluster.name, db_arrayjobs, db_singlejobs
                        )
                        free_slots -= 1
                    if (not db_singlejobs and not db_arrayjobs) or free_slots < 1:
                        keep_queuing = False
            else:
                # TODO Update in DB as Transferred = FINAL state ################ IMPORTANT ############# IMPORTANT ##################
                self.log.debug(f' + {cluster_name:10} > {self.project.name:15} > no transfer required...')
                pass
        except Exception as err:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.log.error(f' {cluster_name:10} > {self.project.name:15} EXCEPTION in threaded validation: {err} '
                           f'[{exc_type} {fname}:{exc_tb.tb_lineno}]')
            if db_arrayjobs_all or db_singlejobs_all:
                self.db_unlock_jobs_in_validation(db_arrayjobs_all, db_singlejobs_all)
        finally:
            self.db.disconnect()
            cluster.disconnect()
            if self.threadlock:
                self.threadlock.release()


    def validate_jobs(self, cluster, active_jobs):
        """Validate jobs"""

        self.log.debug(f' {cluster.name:10} > checking job states...')
        validator = ProjectValidator(db=self.db)
        finished, running = validator.check_finished_jobs(active_jobs, cluster)

        # retrieve job_info files for finished jobs
        finished_ok, finished_failed = ProjectValidator.check_finished_job_info(finished, cluster, self.project)

        if len(running) > 0:
            self.log.info(f' + {cluster.name:10} > updating {len(running):d} running jobs')
            self.db_update_jobs_statuses(running)
        if len(finished_ok) > 0:
            self.log.info(f' + {cluster.name:10} > updating {len(finished_ok):d} finished jobs')
            self.db_update_jobs_statuses(finished_ok)
        if len(finished_failed) > 0:
            failed_final = []
            failed_restarted = []
            for job in finished_failed:
                if job.restarts < int(self.config.get_property('clubber', 'max.job.restarts')):
                    self.restart_failed_job(job, cluster)
                    failed_restarted.append(job)
                else:
                    job.daemon = DaemonState.FAILED.value
                    job.status = JobState.FAILED.value
                    failed_final.append(job)
            self.log.info(f' + {cluster.name:10} > restarted {len(failed_restarted):d} failed jobs')
            if len(failed_final) > 0:
                for _ in failed_final:
                    self.db_update_jobs_statuses(failed_final)
            self.log.info(f' + {cluster.name:10} > marked {len(failed_final):d} failed jobs')
        self.log.debug(f' + {cluster.name:10} > done: checking job states')
        return running, finished_ok, finished_failed

    def cleanup(self, cluster_name, project_job_id):
        cluster = Cluster(cluster_name)

        # noinspection PyBroadException
        try:
            # if self.threadlock:
            #     self.threadlock.acquire()
            cluster.connect()
            self.db.connect()
            self.log.debug(f' + {cluster_name:10} > {self.project.name:15} > {project_job_id:7d} > cleanup...')
            # array jobs
            for arrayjobid in self.db_get_project_job_array_jobs_ids(project_job_id):
                cluster.delete_remote_array_job_results(self.project, arrayjobid)
            # single jobs
            for singlejobid in self.db_get_project_job_single_jobs_ids(project_job_id):
                cluster.delete_remote_job_results(self.project, singlejobid)
            # input
            cluster.delete_remote_project_input(self.project, project_job_id)
            self.log.debug(f' + {cluster_name:10} > {self.project.name:15} > {project_job_id:7d} > done: cleanup')
        except Exception as err:
            self.log.error(f' {cluster_name:10} > {self.project.name:15} > {project_job_id:7d} '
                           f'EXCEPTION in threaded cleanup: {err}')
        finally:
            # if self.threadlock:
            #     self.threadlock.release()
            self.db.disconnect()
            cluster.disconnect()

    def add_cluster(self, cluster, priority=None):
        """Add cluster to the daemon"""

        if priority is None:
            priority = len(self.cluster_list)
        self.cluster_list.insert(priority, cluster)

    def register_jobs(self, cluster_name, jobs_to_submit):
        """Submit jobs in thread"""

        cluster = Cluster(cluster_name)
        cluster.connect()
        self.db.connect()

        # file transfer (input files)
        self.log.debug(f' + {cluster.name:10} > compiling list of inputfiles')
        jobs_per_projid = {}
        files_per_projid = {}
        for job in jobs_to_submit:
            if job.project.subid not in jobs_per_projid.keys():
                jobs_per_projid[job.project.subid] = [job]
                files_per_projid[job.project.subid] = []
            else:
                jobs_per_projid[job.project.subid].append(job)
            if job.file:
                files_per_projid[job.project.subid].append(job.file + "\n")

        for project_subid, jobs in jobs_per_projid.items():
            job_files = files_per_projid[project_subid]

            if len(job_files) > 0:
                self.log.info(
                    f' + {cluster.name:10} > {self.project.name:15} > {project_subid:7d} > '
                    f'sending {len(job_files):d} files'
                )
                tmp_dir = os.path.join(
                    os.path.join(
                        os.path.join(self.project.local_working_dir, self.project.name), str(project_subid)
                    ), 'tmp'
                )
                base_inputdir = self.project.get_local_input_dir(project_subid)
                filelist_file = os.path.join(tmp_dir, '%s.in' % stringutils.get_random_string(16))

                with open(filelist_file, 'w') as f:
                    f.writelines(job_files)
                os.chmod(filelist_file, 0o775)

                remote_destination = os.path.join(self.project.get_absolute_input_dir(cluster),
                                                  str(project_subid)+'/')

                cluster.ssh.send_shell(f'mkdir -p {remote_destination}')

                if cluster.ssh.auth_type == 'idrsa':
                    bash_command_rsync = f'rsync -azqe ' \
                                         f'"ssh -o StrictHostKeyChecking=no -p {int(cluster.ssh.cfg["port"]):d} ' \
                                         f'-i {cluster.ssh.cfg["key_filename"]}" ' \
                                         f'--files-from={filelist_file} {base_inputdir} ' \
                                         f'{cluster.ssh.cfg["username"]}@{cluster.ssh.cfg["hostname"]}:' \
                                         f'{remote_destination}'
                    self.log.debug(bash_command_rsync)
                    subprocess.call(bash_command_rsync, shell=True)
                else:
                    # bash_command_rsync = f'rsync -azqe "ssh -o StrictHostKeyChecking=no" ' \
                    #                      f'--files-from={filelist_file} '\
                    #                      f'{base_inputdir} {cluster.ssh.cfg["username"]}@{cluster.ssh.cfg[' \
                    #                      f'"hostname"]}:{remote_destination} '
                    # bash_command_rsync = [
                    #     'rsync', '-azq', '--files-from=%s', filelist_file, base_inputdir,
                    #     f'{cluster.ssh.cfg["username"]}@{cluster.ssh.cfg["hostname"]}:{remote_destination}']
                    # p= subprocess.Popen(bash_command_rsync, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

                    # self.log.debug(bash_command_rsync)
                    # rsync = pexpect.spawn(bash_command_rsync)
                    # rsync.expect("password:")
                    # rsync.sendline(cluster.ssh.cfg['password'])

                    # TODO fix rsync bug
                    cluster.ssh.sftp_open()
                    for f in job_files:
                        cluster.ssh.sftp_put(os.path.join(base_inputdir, f.rstrip()),
                                             f'{remote_destination}/{f.rstrip()}')

                self.log.debug(f' + {cluster.name:10} > {self.project.name:15} > {project_subid:7d} > all files sent')

            project_job = self.db_get_project_job(project_subid)
            for job in jobs:
                job.project_job = project_job

            self.log.info(
                f' + {cluster.name:10} > {self.project.name:15} > {project_subid:7d} > '
                f'submitting {len(jobs):d} new jobs'
            )
            self.submit_array_job(jobs, cluster)

        cluster.disconnect()
        self.db.disconnect()
