"""Interactive console to submit jobs"""

import sys
import os
import datetime
import time

from .. import config
from .cluster import Cluster, ClusterManager, Project, Job
from .submitter import Submitter
from .validation import ProjectValidator


__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class Console:
    """Interactive console to submit and manage jobs using the automated submission system"""

    def __init__(self):
        self.exit = False
        self.config = config.Config()
        self.clusters_selected = []
        self.clusters = []
        self.auto_balance_threshold = 2
        self.project_name = None

    @staticmethod
    def print_main_menu():
        print("")
        print("+-----------------------------------------------------+")
        print("|   Automated Load Balanced clUster Manager (ALBUM)   |")
        print("|                Interactive  Console                 |")
        print("|                                                     |")
        print("|   Options:                                          |")
        print("|            [1] -  Select cluster(s)                 |")
        print("|            [2] -  Set Auto Balance Threshold        |")
        print("|            [3] -  Create Project                    |")
        print("|            [4] -  Select Project                    |")
        print("|            [5] -  Delete Project                    |")
        print("|            [6] -  Submit Job(s)                     |")
        print("|            [7] -  Check Job(s) state                |")
        print("|            [8] -  Delete Job(s)                     |")
        print("|            [9] -  Retreive Job(s) result            |")
        print("|                                                     |")
        print("|           [10] -  help                              |")
        print("|                                                     |")
        print("|            [0] -  exit                              |")
        print("|                                                     |")
        print("|      $[option] -  access from sub-menus             |")
        print("|             $$ -  go back to main menu any time     |")
        print("|                                                     |")
        print("+-----------------------------------------------------+")
        print("")

    @staticmethod
    def print_quit():
        print("[Console]$ Goodbye...")

    def quit(self):
        self.__clusters_disconnect()
        self.print_quit()

    def __clusters_init(self):
        self.clusters = []
        for cluster_name in self.clusters_selected:
            self.clusters += [Cluster(cluster_name)]
        self.__clusters_connect()

    def __clusters_connect(self):
        for cluster in self.clusters:
            print('[Console]$ >>> Connecting to [%s] ...' % cluster.name)
            cluster.connect()

    def __clusters_disconnect(self):
        for cluster in self.clusters:
            cluster.disconnect()
            print('[Console]$ >>> Disconnected from [%s] ...' % cluster.name)

    @staticmethod
    def __input_check(argument):
        try:
            option = str(argument)
            if option.startswith("$"):
                return False
            else:
                return True
        except ValueError:
            return True
        except TypeError:
            return True

    def menu_action(self, option, forward=None):
        if option == 0:
            self.exit = True
            return forward
        if option == 1:
            available_clusters = Cluster.get_enabled_clusters_from_config()
            cluster_str = 'Available clusters: '
            cluster_str += '%s [default]' % available_clusters[0].name
            for cluster in available_clusters[1:]:
                cluster_str += ', %s' % cluster.name
            print('[Console]$ >>> %s' % cluster_str)
            arg = input('[Console]$ >>> Enter clusters (cluster1[,cluster2,...]): ')
            if self.__input_check(arg):
                self.clusters_selected = arg.split(',')
            else:
                return arg[1:]
            self.__clusters_disconnect()
            print('[Console]$ >>> Clusters: [%s]' % ','.join(self.clusters_selected))
            self.__clusters_init()
            return forward
        if option == 2:
            arg = input('[Console]$ >>> Current threshold is %i, set: ' % self.auto_balance_threshold)
            if self.__input_check(arg):
                self.auto_balance_threshold = int(arg)
            else:
                return arg[1:]
            print('[Console]$ >>> New threshold: %i' % self.auto_balance_threshold)
            return forward
        if option == 3:
            arg = input('[Console]$ >>> Enter project name: ')
            if self.__input_check(arg):
                self.project_name = arg
            else:
                return arg[1:]
            arg = input('[Console]$ >>> Enter script location: ')
            if self.__input_check(arg):
                project_script = arg
            else:
                return arg[1:]
            if not os.path.isfile(project_script):
                print('[Console]$ >>> ERROR! Could not find script file: %s' % project_script)
                return forward
            project_script_name = os.path.basename(project_script)
            if len(self.clusters) == 0:
                print('[Console]$ >>> ERROR! No clusters selected.')
                return forward
            for cluster in self.clusters:
                print('[Console]$ >>> accessing %s ...' % cluster.name)
                cluster.connect()
                cluster.ssh.send_shell('mkdir %s/%s' % (cluster.basedir, self.project_name))
                cluster.ssh.sftp_open()
                cluster.ssh.sftp_put(project_script, f'{cluster.basedir}/{self.project_name}/{project_script_name}')
                cluster.ssh.send_shell(f'chmod 744 {cluster.basedir}/{self.project_name}/{project_script_name}')
            print('[Console]$ >>> Project %s created!' % self.project_name)
            return forward
        if option == 4:
            arg = input('[Console]$ >>> Current project is %s, set: ' % self.project_name)
            if self.__input_check(arg):
                self.project_name = arg
            else:
                return arg[1:]
            print('[Console]$ >>> New project: %s' % self.project_name)
            return forward
        if option == 5:
            arg = input('[Console]$ >>> Enter name of project to delete: ')
            if self.__input_check(arg):
                project_delete = arg
            else:
                return arg[1:]
            arg = input(
                f'[Console]$ >>> Should project {project_delete} be removed from all selected clusters '
                f'[{",".join(self.clusters_selected)}]? (yes/no): '
            )
            if self.__input_check(arg):
                confirm = arg
            else:
                return arg[1:]
            if confirm == 'yes':
                for cluster in self.clusters:
                    print('[Console]$ >>> accessing %s ...' % cluster.name)
                    cluster.connect()
                    cluster.ssh.send_shell('rm -rf %s/%s' % (cluster.basedir, self.project_name))
                print('[Console]$ >>> Project %s deleted!' % project_delete)
            return forward
        if option == 6:
            if len(self.clusters) != 1:
                self.menu_action(1, 6)

            cm = ClusterManager()

            print('[Console]$ +-- Available job parameters ------------------------------------------------------------'
                  '----------------------------------------------------------+')
            print('           |                                                                                        '
                  '                                                          |')
            for _, description in cm.options.items():
                d = description.split('|')
                print('           | %s: %s |' % ("{:<42}".format(d[0]), "{:<100}".format(d[1])))
            print('           |                                                                                        '
                  '                                                          |')
            print('           +-- Job format --------------------------------------------------------------------------'
                  '----------------------------------------------------------+')
            print('           |                                                                                        '
                  '                                                          |')
            print('           |  --param1=... --param2="arg1 arg2" --switch1 --script=... [--array];--param1=... '
                  '--script=... --args="arg1 arg2" [--array]                       |')
            print('           |                                                                job1;job2               '
                  '                                                          |')
            print('           +----------------------------------------------------------------------------------------'
                  '----------------------------------------------------------+')
            print('[Console]$ >>> Current project: %s' % self.project_name)

            arg = input('[Console]$ >>> Enter job(s): ')
            if self.__input_check(arg):
                jobs_args = arg
            else:
                return arg[1:]

            project = None
            if self.project_name is not None:
                project = Project(self.project_name)

            jobs = list()
            for job_cmd in jobs_args.split(";"):
                jobid_check = Job(project=project, cmd=job_cmd)
                jobs += [jobid_check]

            job_count = len(jobs)

            if job_count == 0:
                print('[Console]$ >>> ERROR! No jobs to sumbit.')
                return forward

            print('[Console]$ >>> Submitting %i job(s)...' % job_count)

            if job_count == 1:
                jobid_check = jobs[0]
                s = Submitter(project=project, cluster_list=self.clusters)
                s.submit_job(jobid_check)
                print('[Console]$ >>> Submitted job id: %s to [%s]' % (jobid_check.job_id, jobid_check.cluster))

            return forward
        if option == 7:
            if len(self.clusters) != 1:
                self.menu_action(1, 7)

            if self.project_name is None:
                self.menu_action(4, 7)

            arg = input('[Console]$ >>> Enter job-id(s) seperated by ",": ')
            if self.__input_check(arg):
                jobs_to_check = arg
            else:
                return arg[1:]

            selected_cluster = self.clusters[0]
            cluster_job_list = selected_cluster.get_all_jobs()
            for jobid_check in jobs_to_check.split(","):
                found = False
                for active_job in cluster_job_list:
                    if active_job.job_id == jobid_check:
                        print(
                            f'[Console]$ >>> Job [{jobid_check}] on [{selected_cluster.name}] - '
                            f'status: {active_job.status}')
                        found = True
                        break
                if not found:
                    validator = ProjectValidator(self.project_name)
                    j = Job()
                    j.job_id = int(jobid_check)
                    has_result = validator.validate_job_result(selected_cluster, j)
                    if has_result:
                        print('[Console]$ >>> Job [%s] is finished' % jobid_check)
                    else:
                        print('[Console]$ >>> ERROR! Job [%s] not found.' % j.job_id)

            return forward
        if option == 8:
            if len(self.clusters) != 1:
                self.menu_action(1, 7)

            if self.project_name is None:
                self.menu_action(4, 7)

            selected_cluster = self.clusters[0]
            arg = input('[Console]$ >>> Enter job-id(s) seperated by ",": ')
            if self.__input_check(arg):
                jobs_to_delete_str = arg
            else:
                return arg[1:]
            jobs_to_delete = jobs_to_delete_str.split(",")
            selected_cluster.delete_jobs(jobs_to_delete)
            print('[Console]$ >>> Marked job(s) for deletion: [%s]' % ','.join(jobs_to_delete))
            # TODO delete result files if available
            return forward
        if option == 9:
            if len(self.clusters) != 1:
                self.menu_action(1, 7)

            if self.project_name is None:
                self.menu_action(4, 7)

            cluster = self.clusters[0]
            arg = input('[Console]$ >>> Enter job-id(s) seperated by ",": ')
            if self.__input_check(arg):
                jobs_to_get = arg
            else:
                return arg[1:]
            jobs_to_get_list = ''
            for jobid_check in jobs_to_get.split(","):
                job_details = jobid_check.split(cluster.load_manager.jobid_seperator)
                job_str = jobid_check
                if len(job_details) > 1:
                    job_str = '%s%s%s' % (job_details[0], cluster.load_manager.output_seperator, job_details[1])
                jobs_to_get_list += f'{cluster.basedir}/{self.project_name}' \
                                    f'/{self.config.get_property("clubber", "remote.output.dir")}/{job_str} '
            timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M-%S')
            tmp_archive = '%s.tar.gz' % timestamp
            destination = os.getcwd()
            cluster.connect()
            cluster.ssh.send_shell(f'tar -czf {cluster.basedir}/{self.project_name}/{tmp_archive} {jobs_to_get_list}')
            cluster.ssh.sftp_open()
            cluster.ssh.sftp_get(f'{cluster.basedir}/{self.project_name}/{tmp_archive}', f'{destination}/{tmp_archive}')
            print(f'[Console]$ >>> Saved job(s) at: {destination}/{tmp_archive}')
            cluster.ssh.send_shell(f'rm -f {cluster.basedir}/{self.project_name}/{tmp_archive}')
            return forward
        if option == 10:
            Console.print_main_menu()
            return forward


def main(args):
    """Called when executed from shell"""

    Console.print_main_menu()
    str(args)

    c = Console()
    forward_option = None
    while not c.exit:
        if forward_option is None:
            option = input("[Console]$ ")
        else:
            option = forward_option
            forward_option = None
        try:
            menu_option = int(option)
        except ValueError:
            menu_option = -1
            if option == "$$":
                continue
        if menu_option >= 0:
            forward_option = c.menu_action(menu_option)

    c.quit()


if __name__ == "__main__":
    main(sys.argv[1:])
