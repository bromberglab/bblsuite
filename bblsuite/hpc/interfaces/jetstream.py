"""Interface to connect to Jetstream compute cloud"""


__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class JETSTREAMManager:
    """Manager implementation to execute submissions on the jetstream compute cloud"""

    running = '?'
    pending = '?'
    suspended = '?'

    output_seperator = "."
    jobid_seperator = "_"

    def __init__(self):
        self.cmd_queue = 'squeue'
        self.cmd_submit = 'sbatch'
        self.cmd_delete = 'scancel'
        self.cmd_accounting = 'sacct'
        self.cmd_info = 'sinfo'
        self.__init_options()
        self.input_pattern = "(?:[^\s\"']+\"[^\"]*\"|[^\s\"]+)"

    def __init_options(self):
        """Init JETSTREAM representations of submit options"""

        self.options = {}
        self.__add_option('job_name', '--job-name=')
        self.__add_option('working_dir', '--workdir=')
        self.__add_option('stdout_file', '--output=')
        self.__add_option('stderr_file', '--error=')
        self.__add_option('binary', '--wrap=')
        self.__add_option('script', None)
        self.__add_option('args', None)
        self.__add_option('array', '--array=')

    def __add_option(self, key, val):
        """Add JETSTREAM representation of submit option"""

        self.options[key] = val

    def get_option(self, key):
        """Get JETSTREAM representation of submit option"""

        return self.options[key]

    def parse_submit_command(self, job, extension=None, array_base=None):
        """Return the correct submission format for JETSTREAM representing given options

                <extension> is added to the command as is"""
        return ''

    def get_cmd_jobs_of_user(self, user="*", filter_flags=None):
        """Get jobs of all users or all users if no user specified; optional filter by status"""

        return ''

    def get_cmd_cluster_info(self, user=None):
        """Use ??? to get cluster info"""

        return ''

    @staticmethod
    def get_pending_count(job):
        """Get pending count"""

        return JETSTREAMManager.get_pending_info(job)[0]

    @staticmethod
    def get_pending_info(job):
        """Get pending state, check for pending array jobs

        ...
        """

        # pattern = r'([0-9]+)-([0-9]+):[0-1]'
        # matched = re.match(pattern, job.array_info)
        # if matched is not None:
        #     array_start = int(matched.group(1))
        #     array_end = int(matched.group(2))
        #     array_queued = array_end - (array_start - 1)
        #     return array_queued, (array_start, array_end)
        return 1, None

    def parse_queue(self, queue_out):
        """Parse queue output:

        ...
        """

        return []

    def parse_info(self, raw):
        """Parse info output:

        ...
        """

        info = {
            'nodes_summary': {'allocated': 0, 'mixed': 0, 'other': 0, 'total': 0},
            'cpus_summary': {'allocated': 0, 'idle': 0, 'other': 0, 'total': 0},
            'nodes': []
        }

        return ''

    @staticmethod
    def parse_submit_output(output):
        """Parsing JETSTREAM output of submit call:

        ...
        """

        return 0
