"""clubber"""

import json
import traceback
from ..network import mysql
from .cluster import Cluster
from .submitter import Submitter


__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


def create_project(_project_id):
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.create_project(project_id=_project_id)
    db.disconnect()


def delete_project(_project_id):
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.delete_project(project_id=_project_id, local_delete=True, remote_delete=True, db_delete=True)
    db.disconnect()


def create_project_job(_job_file):
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.create_project_job(project_id=None, project_job_id=None, jobfile_path=_job_file)
    db.disconnect()


def delete_project_job(_project_id, _project_job_id):
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.delete_project_job(project_id=_project_id, project_job_id=_project_job_id,
                                 local_delete=True, remote_delete=True, db_delete=True)
    db.disconnect()


def restart_job(_project_id, _project_job_id, _job_id):
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.restart_job(project_id=_project_id, project_job_id=_project_job_id, job_db_id=_job_id,
                          local_delete=True, remote_delete=False)
    db.disconnect()


def cluster_info(cluster_alias, cluster_actions, json_export=False):
    res = {}
    errors = []
    cluster = Cluster(cluster_alias)
    if not cluster.inited:
        errors.append('no cluster configuration found for alias: %s' % cluster_alias)
        return res
    try:
        cluster.connect()
        for action in cluster_actions.split(','):
            valid = False
            if action == 'workload':
                res[action] = cluster.get_workload()
                valid = True
            if action == 'storage':
                res[action] = cluster.get_disk_usage()
                valid = True
            if action == 'shell':
                res[action] = cluster.get_shell()
                valid = True
            if action == 'connection':
                res[action] = cluster.is_conntectable()
                valid = True
            if not valid:
                res[action] = 'not available'
        cluster.disconnect()
        res['errors'] = errors
        if json_export:
            jenc = json.JSONEncoder()
            return jenc.encode(res)
        else:
            return res
    except Exception:
        message = traceback.format_exc()
        message_list = message.split('\n')
        if message_list[-1].strip():
            errors.append(message_list[-1].strip())
        elif len(message_list) > 1:
            if message_list[-2].strip():
                errors.append(message_list[-2].strip())
        else:
            errors.append('undefined error')
        res['errors'] = errors
        if json_export:
            jenc = json.JSONEncoder()
            return jenc.encode(res)
        else:
            return res
