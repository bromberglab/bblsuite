import os
import sys
import subprocess
from .. import logger, config
from ..network import mysql


LOGGER = logger.Logger.get_logger()


def parse_params(params):
    params_ = {}
    params_['args'] = []
    for p in params:
        p_ = p.split("=")
        if len(p_) == 1:
            params_['args'].append(p)
        else:
            params_[p_[0].replace('--', '')] = p_[1]
    return params_


def get_priority(celery_queue):
    return 100 if celery_queue and "priority" in celery_queue else 0


def check_restart(action, job, id_, celery_, scope):
    id_restarts = []
    job_restarts = []
    if action.endswith('-restart'):
        celery_ = True
        if scope:
            id_restarts += scope
        else:
            job_restarts += job.split(",") if job else []
            for i in id_.split(","):
                if i.find("-") != -1:
                    from_, to_ = i.split("-")
                    id_restarts += list(range(int(from_), int(to_) + 1))
                else:
                    id_restarts += [int(i)]
            if len(id_restarts) == 1:
                id_ = id_restarts[0]
    else:
        if id_:
            id_ = int(id_)
    return id_restarts, job_restarts, id_, celery_
    

def get_scratch_dir(id_, config):
    return os.path.join(
        config.get_property('Service', 'project.scratch'), str(id_) if id_ else ''
    )


def parse_inputfiles(inputfile, filelist):
    input_list = []
    if inputfile:
        input_list.append(inputfile)
    elif filelist:
        with open(filelist) as fin:
            for line in fin.readlines():
                line = line.strip()
                if line:
                    input_list.append(line)
    return input_list


def parse_scope(params, service_db, batch_prefix, jobs_table):
    if 'ids' in params.keys():
        scope = [int(x) for x in params['ids'].split(',')]
    elif 'from_id' in params.keys() and 'to_id' in params.keys():
        scope = range(int(params['from_id']), int(params['to_id']) + 1)
    elif 'submission' in params.keys():
        scope = get_db_ids(params['submission'], service_db, batch_prefix, jobs_table)
    else:
        scope = []
    return scope


def check_result_exists(scope, results_base, resultfile_name):
    scope_invalid_idx = []
    for idx in scope:
        result_file = os.path.join(os.path.join(results_base, str(idx)), resultfile_name)
        if not os.path.exists(result_file):
            scope_invalid_idx.append(scope.index(idx))
            LOGGER.warn(f'Result file not found: {result_file}')
    scope_invalid_idx.sort(reverse=True)
    for invalid_idx in scope_invalid_idx:
        scope.pop(invalid_idx)
    return scope_invalid_idx


def get_db_ids(jobid, service_db, batch_prefix, jobs_table):
    parsed_ids = []
    jobid_list = [str(x) for x in jobid.split(',')]
    service_db.connect()
    batch_ids = []
    single_ids = []
    for job in jobid_list:
        if job.startswith(batch_prefix):  
            batch_ids.append(job)
        else:
            single_ids.append(job)
    res_ = []
    for batch_id in batch_ids:
        res_ = service_db.query(f'SELECT id, refers_to FROM {jobs_table} WHERE batchid="{batch_id}"')
    for single_id in single_ids:
        res_ = service_db.query(f'SELECT id, refers_to FROM {jobs_table} WHERE jobid="{single_id}"')
    for res in res_:
        id_ = int(res[1])if res[1] else int(res[0])
        parsed_ids.append(id_)
    service_db.disconnect()
    return parsed_ids


def download_inputfiles(inputfiles, VALID_URLS, VALID_FILETYPES, VALID_ARCHIVETYPES, download_folder=None):
    has_download_folder = True if download_folder else False
    if not has_download_folder:
        download_folder = os.getcwd()
    downloaded = []

    for file in inputfiles:
        if file.startswith(VALID_URLS):
            url = file
            download_file_name = None
            url_in = file.split(",")
            if len(url_in) == 2:
                url = url_in[0]
                download_file_name = url_in[1]
            downloaded_file = download_url(url, VALID_FILETYPES, VALID_ARCHIVETYPES, file_name=download_file_name, out_folder=download_folder)
            if not downloaded_file:
                sys.exit(f"ERROR - Could not download file from url: {file}")
            else:
                downloaded.append(downloaded_file)
        else:
            downloaded.append(file)

    verified = []
    for file in downloaded:
        if not os.path.exists(file):
            if has_download_folder:
                file_at_download_folder = os.path.join(download_folder, os.path.basename(file))
                if os.path.exists(file_at_download_folder):
                    file = file_at_download_folder
        if not os.path.exists(file):
            sys.exit(f"ERROR - Could not find file: {file}")
        else:
            verified.append(file)

    return verified


def check_valid_input(file_name, VALID_FILETYPES, VALID_ARCHIVETYPES):
    for file_type in VALID_FILETYPES:
        if file_name.endswith(file_type):
            return True
    for archive_type in VALID_ARCHIVETYPES:
        if file_name.endswith(archive_type):
            return True
    return False


def run_command(command, shell=False, print_output=True, env_exports={}):
    current_env = os.environ.copy()
    merged_env = {**current_env, **env_exports}
    process = subprocess.Popen(command, shell=shell, env=merged_env, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = []
    for line in iter(process.stdout.readline, b''):
        line = line.rstrip().decode('utf8')
        if print_output:
            print(">>>", line)
        output.append(line)
    return output


def is_download(inputfiles, VALID_URLS, VALID_PREFIXES=()):
    if inputfiles:
        for inputfile in inputfiles:
            if inputfile.startswith(VALID_URLS) or inputfile.startswith(VALID_PREFIXES):
                return True
    return False


def download_url(url, VALID_FILETYPES, VALID_ARCHIVETYPES, file_name=None, out_folder=None):
    import ssl
    import http
    from urllib.request import urlopen
    from urllib.error import HTTPError, URLError

    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    if not file_name:
        file_name = url.rsplit('/', 1)[-1]
        if not check_valid_input(file_name, VALID_FILETYPES, VALID_ARCHIVETYPES):
            return None

    if out_folder:
        if not os.path.exists(out_folder):
            os.makedirs(out_folder, exist_ok=True)
        file_name_path = os.path.join(out_folder, file_name)
    else:
        file_name_path = file_name
    try:
        connection_error = False
        print(f'[ urllib ] Retrieving {file_name}...', end='', flush=True)
        with urlopen(url, context=ctx) as u, open(file_name_path, 'wb') as f:
            f.write(u.read())
        print(' done')
    except http.client.IncompleteRead as e:
        print(' canceled')
        print(f"ERROR: Potential incomplete read ({len(e.partial)} bytes read) from url: {url}")
        connection_error = True
    except HTTPError as e:
        print(' canceled')
        if hasattr(e, 'code'):
            print(f'ERROR: The server could not fulfill the request; Error code: {e.code}')
        connection_error = True
    except URLError as e:
        print(' canceled')
        if hasattr(e, 'reason'):
            print(f'ERROR: We failed to reach the server; Reason: {e.reason}')
        connection_error = True
    
    if connection_error:
        return None

    return file_name_path
