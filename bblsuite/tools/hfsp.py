"""Calculate HSFP scores"""

import math

__author__ = 'Yannick Mahlich'
__email__ = 'ymahlich@bromgberglab.org'


def get_ungapped_aln_len(perc_identity, aln_length, mismatch_count):
    """get_ungapped_aln_len(perc_identity, aln_length, mismatch_count) 
        -> int

    calculates the ungapped alignment length, from over all alignment 
    lenght, sequence identity and mismatch count as determined by 
    PSI-BLAST (or any other alignment Tool). Note that PSI-BLAST counts 
    positive matches as mismatches in the number returned by '--outfmt=6'

    Arguments:
        perc_identity {float} -- sequence identity in . notation i.e.
            0.98 for 98% sequence identity
        aln_length {int} -- alignment length
        mismatch_count {int} -- number of mismatches in the alignment 
            i.e. aligned position that are neither gaps nor identities

    Returns:
        int -- ungapped alignment length
    """
    num_identity = math.ceil(perc_identity * aln_length)
    aln_length_ungapped = num_identity + mismatch_count
    return aln_length_ungapped


def calc_hssp(perc_identity, aln_length_ungapped):
    """calc_hssp(perc_identity, aln_length_ungapped) -> float

    function that caluclates the hssp between two protein pairs as
    determined by the (PSI)BLAST alignment

    Arguments:
        perc_identity {float} -- the percentage of identity (identical
            amino acids) between two proteins in the (PSI-)BLAST alignment
        aln_length {int} -- the alignment length of the (PSI-)BLAST
            alignment between query and target sequence

    Returns:
        float -- the caluclated hssp value
    """

    perc_identity = perc_identity * 100

    if aln_length_ungapped <= 11:
        hssp = -101
    elif aln_length_ungapped > 450:
        hssp = perc_identity - 19.5
    else:
        exp = -0.32 * (1 + math.exp(-aln_length_ungapped / 1000))
        hssp = perc_identity - (480 * math.pow(aln_length_ungapped, exp))

    return round(hssp, 4)


def calc_hfsp(perc_identity, aln_length_ungapped):
    """calc_hfsp(perc_identity, aln_length_ungapped) -> float

    function that caluclates the hfsp between two protein pairs as
    determined by the MMSeqs2 alignment

    Arguments:
        perc_identity {float} -- the percentage of identity (identical
            amino acids) between two proteins in the MMSeqs2 alignment
        aln_length {int} -- the alignment length of the MMSeqs2
            alignment between query and target sequence

    Returns:
        float -- the caluclated hfsp value
    """

    perc_identity = perc_identity * 100

    if aln_length_ungapped <= 22:
        hfsp = -101
    elif aln_length_ungapped > 450:
        hfsp = perc_identity - 28.4
    else:
        exp = -0.33 * (1 + math.exp(-aln_length_ungapped / 1000))
        hfsp = perc_identity - (770 * math.pow(aln_length_ungapped, exp))

    return round(hfsp, 4)
