import random
import string


def get_random_string(length=16):
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])


def is_subseq(x, y):
    it = iter(y)
    return all(c in it for c in x)


def ec_numbers(ec):
    return [int(_.replace('n', '')) for _ in ec[0].split(".")]
