import os
import hashlib
from pathlib import Path


def makedir(path, parents=False, mode=0o777, umask=False, exist_ok=False):
    if umask is not False:
        current_mask = os.umask(umask)
    Path(path).mkdir(mode=mode, parents=parents, exist_ok=exist_ok)
    if umask is not False:
        os.umask(current_mask)


def find_in_dir(root_dir, extensions=None, correct=False):
    found_files = []
    for root, _, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename[0] == '.':
                continue
            file = os.path.join(root, filename)
            valid, filename_corrected = check_filetype(filename, extensions, correct)
            if valid:
                if filename_corrected:
                    file_old = file
                    file = os.path.join(root, filename_corrected)
                    os.rename(file_old, file)
                found_files.append(file)
    return found_files


def check_filetype(filename, extensions=None, correct=False):
    valid = False
    filename_corrected = None
    if not extensions:
        valid = True
    else:
        for ext in extensions:
            if filename.endswith(f'.{ext}'):
                valid = True
                return valid, filename_corrected
        if correct:
            for ext in extensions:
                if ext in filename:
                    filename_corrected = f'{filename}.corrected.{ext}'
                    valid = True
                    return valid, filename_corrected
    return valid, filename_corrected


def hashfile(path, blocksize = 65536):
    afile = open(path, 'rb')
    hasher = hashlib.md5()
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    afile.close()
    return hasher.hexdigest()


def findDup(self):
        # Dups in format {hash:[names]}
        dups = {}
        for dirName_src, _, fileList_src in os.walk(self.out_folder):
            print('Scanning %s...' % dirName_src)
            for filename_src in fileList_src:
                # Get the path to the file
                path_src = os.path.join(dirName_src, filename_src)
                # Calculate hash
                file_hash_src = hashfile(path_src)
                # Add or append the file path
                if file_hash_src in dups:
                    print('Duplicate %s' % path_src)
                dups[file_hash_src] = [path_src]

        for dirName_target, _, fileList_target in os.walk(self.ppcache):
            print('Scanning %s...' % dirName_target)
            for filename_target in fileList_target:
                # Get the path to the file
                path_target = os.path.join(dirName_target, filename_target)
                # Calculate hash
                file_hash_target = hashfile(path_target)
                # Add or append the file path
                if file_hash_target in dups:
                    print('Duplicate %s <-> %s' % (dups[file_hash_target], file_hash_target))
                    dups[file_hash_target].append(path_target)
                else:
                    dups[file_hash_target] = [path_target]
        return dups
