import argparse
import os.path
import math

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


DEFAULT_SEPARATOR = '_'

prefix = None
separator = None


def file_len(fname):
    i = -1
    with open(fname) as f_:
        for i, l in enumerate(f_):
            pass
    return i + 1


def fasta_header_count(fname):
    header_count = 0
    with open(fname) as f_:
        for i, l in enumerate(f_):
            if l[0] == ">":
                header_count += 1
    return header_count


def split_by_length(fastafile, outfolder, splitlength):
    split_idx = 1
    out_files = []
    total_splitted = 0
    input_file = open(fastafile, 'r')
    split_out_base = outfolder
    if prefix:
        split_out_base = os.path.join(split_out_base, prefix)
    else:
        split_out_base = os.path.join(split_out_base, os.path.splitext(os.path.basename(fastafile))[0])
    if separator:
        split_out_base += separator
    else:
        split_out_base += DEFAULT_SEPARATOR

    reading = True
    while reading:
        first = True
        split_out_filename = "%s%s.fasta" % (split_out_base, split_idx)
        output_file = open(split_out_filename, 'w')
        out_files.append(split_out_filename)
        current_splitted = 0
        while current_splitted < splitlength:
            line = input_file.readline()
            while not line.strip():
                line = input_file.readline()
                if len(line) == 0:
                    break
            header = line
            current_splitted += 1
            sequence = input_file.readline().rstrip()
            if not header.rstrip() and not sequence:
                reading = False
                if first:
                    output_file.close()
                    os.remove(split_out_filename)
                    split_idx -= 1
                    out_files.pop(-1)
                break
            last_pos = input_file.tell()
            line = input_file.readline().rstrip()
            while line and line[0] != '>':
                sequence += line
                last_pos = input_file.tell()
                line = input_file.readline().rstrip()
            if line and line[0] == '>':
                input_file.seek(last_pos)
            output_file.write(header + sequence + "\n")
            total_splitted += 1
            first = False
        output_file.close()
        split_idx += 1
    return out_files, total_splitted


# DEPRECATED; UNUSED; NEEDS a bugfix
def split_by_length_2line_format(fastafile, outfolder, splitlength):
    split_idx = 1
    out_files = []
    input_file = open(fastafile, 'r')
    split_out_base = outfolder
    if prefix:
        split_out_base = os.path.join(split_out_base, prefix)
    else:
        split_out_base = os.path.join(split_out_base, os.path.splitext(os.path.basename(fastafile))[0])
    if separator:
        split_out_base += separator
    else:
        split_out_base += DEFAULT_SEPARATOR

    reading = True
    while reading:
        first = True
        split_out_filename = "%s%s.fasta" % (split_out_base, split_idx)
        output_file = open(split_out_filename, 'w')
        out_files.append(split_out_filename)
        for lines in range((split_idx - 1) * splitlength, split_idx * splitlength):
            header = input_file.readline()
            sequence = input_file.readline()
            if not header.rstrip() and not sequence.rstrip():
                reading = False
                if first:
                    output_file.close()
                    os.remove(split_out_filename)
                    split_idx -= 1
                    out_files.pop(-1)
                break
            output_file.write(header + sequence)
            first = False
        output_file.close()
        split_idx += 1
    return out_files


def split_by_number(fastafile, outfolder, splitnumber):
    total_entries = fasta_header_count(fastafile) / 2
    splitlength = math.floor(total_entries / splitnumber)
    return split_by_length(fastafile, outfolder, splitlength)


def get_argument_parser():
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='Split a fasta file either by number of sequences or total splits')

    parser.add_argument('--input', '-i', metavar='inputfile.fasta', required=True, type=str,
                        help='file in fasta format to be split')

    parser.add_argument('--outfolder', '-o', metavar='outfolder', required=True, type=str,
                        help='folder to write splitted files to')

    parser.add_argument('--prefix', '-p', metavar='prefix', type=str,
                        help='prefix for the created split files; default: filename')

    parser.add_argument('--separator', '-s', metavar='separator', default='_', type=str,
                        help='separator to be used between base filname and split number; default: "_"')

    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('--splitlength', '-l', metavar='split_length', type=str,
                       help='number of single fasta formatted sequences to include in each split file')

    group.add_argument('--splitnumber', '-n', metavar='split_number', type=str,
                       help='number of desired total splits')

    return parser


def main(infile, splitlength, outfolder, splitnumber=None):
    if not os.path.exists(outfolder):
        os.mkdir(outfolder)

    if splitlength:
        return split_by_length(infile, outfolder, int(splitlength))
    elif splitnumber:
        return split_by_number(infile, outfolder, int(splitnumber))


if __name__ == "__main__":
    ArgParser = get_argument_parser()
    args = ArgParser.parse_args()

    _infile = args.input
    prefix = args.prefix
    separator = args.separator
    _splitlength = args.splitlength
    _splitnumber = args.splitnumber
    _outfolder = args.outfolder

    split_files = main(_infile, _splitlength, _outfolder, _splitnumber)

    for f in split_files:
        print(f)
