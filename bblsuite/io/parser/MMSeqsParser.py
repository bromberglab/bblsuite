"""Parser for mmseqs2"""

__author__ = 'Yannick Mahlich'
__email__ = 'ymahlich@bromberglab.org'

import sys
import gzip

class MMSeqsParser:

    def __init__(self, inf_path, format):
        self.inf_path = inf_path
        self.format = format

    def __enter__(self):
        if self.format == 'm8':
            self.infile = open(self.inf_path, 'r')
        elif self.format == 'm8.gz':
            self.infile = gzip.open(self.inf_path, 'rt')
        else:
            raise ValueError()
        return self

    def __exit__(self, *args):
        self.infile.close()

    def parse(self):
        if self.format in ['m8', 'm8.gz']:
            try:
                for row in self.infile:
                    s_row = row.rstrip().split('\t')
                    record = {
                        'query': s_row[0],
                        'target': s_row[1],
                        'sequence_identity': float(s_row[2]),
                        'alignment_length': int(s_row[3]),
                        'mismatches': int(s_row[4]),
                        'gaps': int(s_row[5]),
                        'query_start': int(s_row[6]),
                        'query_end': int(s_row[7]),
                        'target_start': int(s_row[8]),
                        'target_end': int(s_row[9]),
                        'evalue': float(s_row[10]),
                        'bitscore': int(s_row[11])
                    }
                    yield(record)
            except IndexError as e:
                print('Error while parsing infile: {}'.format(e), 
                    file=sys.stderr)
        else:
            raise ValueError()
    

