"""Wrapper for MySQL"""

import pymysql
from .. import config, logger

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class MySQLHandler:
    """Communication with a MySQL database"""

    log = logger.Logger.get_logger()

    def __init__(self, db_from_config=None):
        self.conf = config.Config()
        if db_from_config is None:
            self.alias = None
            self.conn = None
            self.hostname = None
            self.username = None
            self.password = None
            self.database = None
            self.port = None
            self.timezone = None
        else:
            self.conn = None
            self.alias = db_from_config.lower()
            self.get_database_config()

    def duplicate(self):
        d = MySQLHandler()
        d.conn = None
        d.alias = self.alias
        d.set_database(self.hostname, self.username, self.password, self.database, self.port, self.timezone)
        return d

    def set_database(self, hostname, username, password, database=None, port=3306, timezone='+00:00'):
        """Manually set database"""

        self.alias = hostname
        self.hostname = hostname
        self.username = username
        self.password = password
        self.database = database
        self.port = port
        self.timezone = timezone

    def get_database_config(self):
        """
        Create database from config.

        Args:

        Returns:
            none
        """

        if self.alias == 'default':
            self.alias = self.conf.get_property('General', 'default.db')
        elif self.alias == 'default-clubber':
            self.alias = self.conf.get_property('clubber', 'database')
        if not self.conf.has_property('Database', '%s.hostname' % self.alias):
            self.log.warn('No config found for database identifier: %s' % self.alias)
        else:
            self.hostname = self.conf.get_property('Database', '%s.hostname' % self.alias)
            self.database = self.conf.get_property('Database', '%s.database' % self.alias)
            self.port = int(self.conf.get_property('Database', '%s.port' % self.alias))
            self.username = self.conf.get_property('Database', '%s.username' % self.alias)
            self.password = self.conf.get_property('Database', '%s.password' % self.alias)
            self.timezone = self.conf.get_property('Database', '%s.timezone' % self.alias)

    def connect(self):
        """Connect to mysql db"""

        if self.conn:
            if not self.conn.open:
                self.log.debug('Reconnecting to database: %s (%s)' % (self.alias, self.hostname))
            try:
                self.conn.ping()
                if self.conn.open: 
                    return self.conn.open
            except pymysql.err.OperationalError as err:
                self.log.error(err)
        else:
            self.log.debug('Connecting to database: %s (%s)' % (self.alias, self.hostname))
        try:
            self.conn = pymysql.connect(host=self.hostname,
                                        user=self.username,
                                        password=self.password,
                                        db=self.database,
                                        cursorclass=pymysql.cursors.DictCursor)
            self.log.debug('Opened connection to database: %s (%s)' % (self.alias, self.hostname))
            self.query('SET time_zone="%s"' % self.timezone)
        except pymysql.err.OperationalError as err:
            self.log.error(err)
        finally:
            if self.conn:
                return self.conn.open
            else:
                return False

    def disconnect(self):
        """Close mysql connection"""

        if self.conn:
            if self.conn.open:
                self.log.debug('Closing connection to database: %s (%s)' % (self.alias, self.hostname))
                self.conn.close()
        return not self.conn.open

    @staticmethod
    def post_query(resultset):
        result = []
        for entry in resultset:
            entry_tuple = ()
            for _, value in entry.items():
                entry_tuple += (value,)
            result.append(entry_tuple)
        return tuple(result)

    def query(self, mysql, args=None):
        """Submit a single mysql query"""

        try:
            self.connect()
            db_cursor = self.conn.cursor()
            if not args:
                db_cursor.execute(mysql)
            else:
                db_cursor.execute(mysql, args)
            resultset = db_cursor.fetchall()
            db_cursor.close()
            result = MySQLHandler.post_query(resultset)
            return result
        except pymysql.MySQLError as e:
            self.log.error('MySQL command: %s' % mysql)
            self.log.error('MySQL error: %s' % e)

    def insert(self, mysql, args=None):
        """Submit a single insert query"""

        try:
            self.connect()
            db_cursor = self.conn.cursor()
            if not args:
                db_cursor.execute(mysql)
            else:
                db_cursor.execute(mysql, args)
            db_cursor.fetchall()
            idx = db_cursor.lastrowid
            db_cursor.close()
            self.conn.commit()
            return idx
        except pymysql.MySQLError as e:
            self.log.error('MySQL command: %s' % mysql)
            self.log.error('MySQL error: %s' % e)

    def insert_many(self, mysql, args=None):
        """Submit a multiple insert query"""

        try:
            self.connect()
            db_cursor = self.conn.cursor()
            if not args:
                db_cursor.executemany(mysql)
            else:
                db_cursor.executemany(mysql, args)
            db_cursor.fetchall()
            idx = db_cursor.lastrowid
            db_cursor.close()
            self.conn.commit()
            return idx
        except pymysql.MySQLError as e:
            self.log.error('MySQL command: %s' % mysql)
            self.log.error('MySQL error: %s' % e)

    def update(self, mysql, args=None):
        """Submit a single update query"""

        try:
            self.connect()
            db_cursor = self.conn.cursor()
            if not args:
                row_count = db_cursor.execute(mysql)
            else:
                row_count = db_cursor.execute(mysql, args)
            db_cursor.close()
            self.conn.commit()
            return row_count
        except pymysql.MySQLError as e:
            self.log.error('MySQL command: %s' % mysql)
            self.log.error('MySQL error: %s' % e)

    def update_on_duplicate_key(self, mysql, args=None):
        """Submit a single update query"""

        try:
            self.connect()
            db_cursor = self.conn.cursor()
            if not args:
                row_count = db_cursor.execute(mysql)
            else:
                row_count = db_cursor.execute(mysql, args)
            db_cursor.close()
            self.conn.commit()
            return row_count
        except pymysql.MySQLError as e:
            self.log.error('MySQL command: %s' % mysql)
            self.log.error('MySQL error: %s' % e)
