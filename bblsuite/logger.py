"""Logging module"""

import os
import sys
import logging
from . import globals
from .config import Config
from .tools.modules import caller_name

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class Logger:

    @staticmethod
    def get_logger(name=None, level=None, file=None, console=False, force_no_handler=False):
        """Create custom logger at given level with optional file handler"""

        # create logger instance
        # to only get name or actual creator class use: logger = logging.getLogger(__name__)
        if not name:
            name = caller_name()
        logger = logging.getLogger(name)

        # only create logger if not instance available for specific class
        if logger.handlers.__len__() == 0:

            # if config has logging directives use those and overwrite current settings
            config = Config()
            if config.config.has_section('Logging'):
                if file is None:
                    if config.has_property('Logging', 'file', fallback=False):
                        file = config.get_property('Logging', 'file')
                        if not file:
                            file = config.DEFAULT_LOG_FILE
                if not level:
                    level = config.DEFAULT_LOG_LEVEL
                    if config.has_property('Logging', 'level'):
                        level = config.get_property('Logging', 'level')

            if force_no_handler:
                # only set logging level
                logger.setLevel(config.DEFAULT_LOG_LEVEL if not level else level)

            elif not globals.QUIET:
                # set logging level
                logger.setLevel(config.DEFAULT_LOG_LEVEL if not level else level)

                # create a logging format
                formatter = CustomFormatter()

                # create a stream handler
                if console or not file:
                    stream_handler = logging.StreamHandler(sys.stdout)
                    stream_handler.setFormatter(formatter)
                    logger.addHandler(stream_handler)

                # create a file handler, fallback to console
                if file:
                    if Logger.check_logfile_access(logger, file):
                        file_handler = logging.FileHandler(file)
                        file_handler.setFormatter(formatter)
                        logger.addHandler(file_handler)
                    elif not console:
                        stream_handler = logging.StreamHandler(sys.stdout)
                        stream_handler.setFormatter(formatter)
                        logger.addHandler(stream_handler)

                logger.debug('Logger attached')
            else:
                # set logging level to error
                logger.setLevel('ERROR')

        return logger

    @staticmethod
    def check_logfile_access(logger, file):
        logfile_accessible = False
        try: 
            with open(file, "a"):
                logfile_accessible = True
        except FileNotFoundError:
            logger.error(
                'Could not attach logging file handler - ' \
                'No such file or directory: ' + str(file)
            )
        except PermissionError:
            logger.error(
                'Could not attach logging file handler - ' \
                'Permission denied: ' + str(file)
            )
        return logfile_accessible

    @staticmethod
    def addFileHandler(logger, file, formatter=None):
        if Logger.check_logfile_access(logger, file):
            if not formatter:
                formatter = CustomFormatter()
            file_handler = logging.FileHandler(file)
            file_handler.setFormatter(formatter)
            logger.addHandler(file_handler)
        else:
            logger.error('Could not attach logging file handler: ' + str(file))


class CustomFormatter(logging.Formatter):
    """Custom log output formatter"""

    info_fmt = '[%(asctime)s] [%(levelname)-8s] : %(message)s'
    warn_fmt = '[%(asctime)s] [%(levelname)-8s] : %(message)s'
    dbg_fmt = '[%(asctime)s] [%(levelname)-8s] : %(message)s'
    err_fmt = '[%(asctime)s] [%(levelname)-8s] %(filename)s:%(lineno)s : %(message)s'

    def __init__(self, fmt="X%(levelno)s: %(msg)s"):
        logging.Formatter.__init__(self, fmt, "%Y-%m-%d %H:%M:%S")

    def format(self, record):
        """Re-format log messages"""

        # Save the original format configured by the user
        # when the logger formatter was instantiated
        format_orig = self._fmt

        # Replace the original format with one customized by logging level

        if record.levelno == logging.DEBUG:
            self._fmt = CustomFormatter.dbg_fmt

        elif record.levelno == logging.INFO:
            self._fmt = CustomFormatter.info_fmt

        elif record.levelno == logging.WARNING:
            self._fmt = CustomFormatter.warn_fmt

        elif record.levelno == logging.ERROR:
            self._fmt = CustomFormatter.err_fmt

        # Python 3 needs the following assignment due to changes in internal logging machanism
        self._style = logging.PercentStyle(self._fmt)

        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)

        # Restore the original format configured by the user
        self._fmt = format_orig

        return result


# override print()
print = Logger.get_logger("print", level="INFO").info
