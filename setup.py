from setuptools import setup, find_packages

# TODO: add proper license
# TODO: add proper urls

setup(
    name="bblsuite",
    version="1.0",
    packages=find_packages(),
    python_requires='>=3.6',
    install_requires=[
        'PyMySQL',
        'paramiko',
        'lxml',
        'python-dotenv',
    ],
    
    # metadata to display on PyPI
    author="Bromberg Lab",
    author_email="services@bromberglab.com",
    description="Bromberg Lab Suite",
    license="PSF",
    # keywords="hello world example examples",
    # url="http://example.com/HelloWorld/",   # project home page, if any
    
    project_urls={
        "Bug Tracker": "https://bugs.example.com/HelloWorld/",
        "Documentation": "https://docs.example.com/HelloWorld/",
        "Source Code": "https://bitbucket.org/bromberglab/bblsuite",
    }

    # could also include long_description, download_url, classifiers, etc.
)